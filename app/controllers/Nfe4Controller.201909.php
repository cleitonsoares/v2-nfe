<?php
/**
 * API NFE
 *
 * @author      Cleiton Soares <cleitonhjs@gmail.com>
 * @copyright   Cleiton Soares
 * @version     0.1
 */

namespace EmissorNfe\Controllers;

use NFePHP\NFe\Make;
use NFePHP\NFe\Tools;
use NFePHP\Common\Certificate;
use NFePHP\Common\Keys;
use NFePHP\NFe\Common\Standardize;
use NFePHP\NFe\Complements;
use NFePHP\NFe\Factories\Contingency;
use NFePHP\NFe\Factories\Protocol;
use EmissorNfe\Helpers\Formata as Formata;
use EmissorNfe\Helpers\Formulario as Formulario;
use EmissorNfe\Helpers\Parametros;
use NFePHP\DA\NFe\Danfe;
use NFePHP\DA\NFe\Daevento;
use NFePHP\DA\Legacy\FilesFolders;
use NFePHP\Mail\Mail;
use PHPUnit\Framework\MockObject\Stub\Exception;

class Nfe4Controller extends RESTController
{

    protected $allowedFields = [
        'search' => ['chave','recibo','apenasconsulta','tipo_retorno','ev','seq','op','preview'],
    ];

    /**
     * Iniciamos variáveis que utilizaremos para salvar logs
     */
    public $empresa;
    public $nota;
    public $arrEnviar;
    public $chave;
    public $nfe;
    public $nroRecibo;
    public $codSefaz;
    public $strMotivo;
    public $aResposta;
    public $objDanfe;
    public $arrNotaLog;
    public $destinatario;
    public $error;
    public $msg;
    public $codSituacao;
    public $retornoCliente;
    public $nroProtocolo;
    public $xmlRetorno;
    public $operacao;
    public $daevento;
    public $arqXmlDanfe;
    public $sequenciaCce;
    public $ehForaEstado;
    public $notaExterior = false;
    public $paisDestino;
    public $seqEvento;
    public $arrParametros;

    public function __construct($parseQueryString = true)
    {

        try {
            $this->_setEmpresa();
        } catch (\Exception $e) {
            return $this->provide(Formulario::getDefaultMessages(ERRO, SELECIONAR, $e->getMessage()));
        }

        parent::__construct($parseQueryString);
    }

    /**
     * Verifica os dados da empresa, como host e porta e seta em objeto
     *
     * @return Mixed
     */
    protected function _setEmpresa()
    {

        $file = $this->_getDirNfe() .'/extras/empresa.json';

        if(file_exists($file)) {
            $this->empresa = json_decode(file_get_contents($file), true);
        } else {
            throw new \Exception("Arquivo de configuração da empresa não encontrado!");
        }

        if($this->empresa['tpAmb'] == NFE_AMBIENTE_PRODUCAO) {

            $this->empresa['pasta_ambiente'] = 'producao';

        } else {
            $this->empresa['pasta_ambiente'] = 'homologacao';
        }
    }

    /**
     * Realiza as configurações iniciais
     * @return mixed
     */
    public function configuraNFe()
    {

        $aDocFormat  = [
            "format"       => "L",
            "paper"        => "A4",
            "southpaw"     => "1",
            "pathLogoFile" => "",
            "logoPosition" => "L",
            "font"         => "Times",
            "printer"      => ""
        ];

        $aMailConf = [
            "mailAuth"         => "1",
            "mailFrom"         => "",
            "mailSmtp"         => "",
            "mailUser"         => "",
            "mailPass"         => "",
            "mailProtocol"     => "",
            "mailPort"         => "",
            "mailFromMail"     => null,
            "mailFromName"     => null,
            "mailReplayToMail" => null,
            "mailReplayToName" => null,
            "mailImapHost"     => null,
            "mailImapPort"     => null,
            "mailImapSecurity" => null,
            "mailImapNocerts"  => null,
            "mailImapBox"      => null
        ];

        $aProxyConf = [
            "proxyIp"   => "",
            "proxyPort" => "",
            "proxyUser" => "",
            "proxyPass" => ""
        ];


        $arr = [
            "atualizacao"        => "2018-07-11 20:10:02",
            "tpAmb"              => (int) $this->empresa['tpAmb'],
            "pathXmlUrlFileNFe"  => "nfe_ws3_mod55.xml",
            "pathXmlUrlFileCTe"  => "/var/www/nfephp/config/cte_ws2.xml",
            "pathXmlUrlFileMDFe" => "/var/www/nfephp/config/mdf2_ws1.xml",
            "pathXmlUrlFileCLe"  => "",
            "pathXmlUrlFileNFSe" => "",
            "pathNFeFiles"       => $this->_getDirNfe(),
            "pathCTeFiles"       => '',
            "pathMDFeFiles"      => '',
            "pathCLeFiles"       => '',
            "pathNFSeFiles"      => '',
            "pathCertsFiles"     => $this->_getDirNfe().'/certs/',
            "siteUrl"            => "http://localhost/nfe",
            "schemes"            => "PL_009_V4",
            "schemesCTe"         => "PL_CTE_104",
            "schemesMDFe"        => "MDFe_100",
            "schemesCLe"         => "CLe_100",
            "schemesNFSe"        => "",
            "razaosocial"        => "Tat",
            "siglaUF"            => $this->empresa['UFEmitente'],
            "cnpj"               => trim($this->empresa['CNPJEmitente']),
            "certPfxName"        => trim($this->empresa['emp_nfe_cert_arq']),
            "certPassword"       => trim($this->empresa['emp_nfe_pwd']),
            "certPhrase"         => "",
            "aDocFormat"         => $aDocFormat,
            "aMailConf"          => $aMailConf,
            "aProxyConf"         => $aProxyConf,
            "versao"             => '4.00',
        ];

        $this->salvarLog('****NFe 4.0**** Chamando Tools com -> '.print_r($arr, true));

        return json_encode($arr);
    }

    /**
     * Lê e retorna os dados do certificado 
     * @return Certificate
     * @throws \Exception
     */
    protected function _getCertificado()
    {

        $certificado = file_get_contents($this->_getDirNfe() . '/certs/' . trim($this->empresa['emp_nfe_cert_arq']));
        try {
            $cert = Certificate::readPfx($certificado, trim($this->empresa['emp_nfe_pwd']));
        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        return $cert;
    }

    protected function _getDirNfe()
    {
        return NFE_DIR;
    }

    /**
     * Inicializa a criaçao dos diretorios de NFe
     */
    protected function _criarDir()
    {
        $dirEmpNfe = $this->_getDirNfe();

        if (!file_exists($dirEmpNfe)) {

            $this->salvarLog('****NFe 4.0**** criando pasta: '.$dirEmpNfe);
            mkdir($dirEmpNfe, 0777, true);
            chmod($dirEmpNfe, 0777);
            mkdir($dirEmpNfe.'/certs', 0777, true);
            mkdir($dirEmpNfe.'/danfe', 0777, true);
            mkdir($dirEmpNfe.'/homologacao', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/assinadas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/canceladas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/entradas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/enviadas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/temporarias', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/enviadas/aprovadas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/enviadas/denegadas', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/cartacorrecao', 0777, true);
            mkdir($dirEmpNfe.'/homologacao/inutilizadas', 0777, true);

            mkdir($dirEmpNfe.'/producao', 0777, true);
            mkdir($dirEmpNfe.'/producao/assinadas', 0777, true);
            mkdir($dirEmpNfe.'/producao/canceladas', 0777, true);
            mkdir($dirEmpNfe.'/producao/entradas', 0777, true);
            mkdir($dirEmpNfe.'/producao/enviadas', 0777, true);
            mkdir($dirEmpNfe.'/producao/temporarias', 0777, true);
            mkdir($dirEmpNfe.'/producao/enviadas/aprovadas', 0777, true);
            mkdir($dirEmpNfe.'/producao/enviadas/denegadas', 0777, true);
            mkdir($dirEmpNfe.'/producao/cartacorrecao', 0777, true);
            mkdir($dirEmpNfe.'/producao/inutilizadas', 0777, true);

            mkdir($dirEmpNfe.'/extras', 0777, true);
            
        } else {

            if(!file_exists($dirEmpNfe.'/danfe')) {

                mkdir($dirEmpNfe.'/danfe', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/assinadas')) {

                mkdir($dirEmpNfe.'/producao/assinadas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/canceladas')) {

                mkdir($dirEmpNfe.'/producao/canceladas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/entradas')) {

                mkdir($dirEmpNfe.'/producao/entradas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/enviadas')) {

                mkdir($dirEmpNfe.'/producao/enviadas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/temporarias')) {

                mkdir($dirEmpNfe.'/producao/temporarias', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/enviadas/aprovadas')) {

                mkdir($dirEmpNfe.'/producao/enviadas/aprovadas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/enviadas/denegadas')) {

                mkdir($dirEmpNfe.'/producao/enviadas/denegadas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/cartacorrecao')) {

                mkdir($dirEmpNfe.'/producao/cartacorrecao', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/producao/inutilizadas')) {

                mkdir($dirEmpNfe.'/producao/inutilizadas', 0777, true);
            }

            if(!file_exists($dirEmpNfe.'/homologacao')) {

                mkdir($dirEmpNfe.'/homologacao', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/assinadas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/canceladas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/entradas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/enviadas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/temporarias', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/enviadas/aprovadas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/enviadas/denegadas', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/cartacorrecao', 0777, true);
                mkdir($dirEmpNfe.'/homologacao/inutilizadas', 0777, true);
            }
        }
    }



    /**
     * Função de criação de uma NF-e. Será a primeira a ser chamada caso tenha que ser enviado a nota pra sefaz
     * ALGUM ROBO CHAMARA ESSA FUNÇAO... OU ALGUM COMPLEMENTO DE ALGUM METODO
     */
    public function criarNfe()
    {

//        var_dump(OPENSSL_VERSION_TEXT);
//        var_dump(OPENSSL_VERSION_NUMBER);
//        print_r(curl_version());die;

        $arrParam = json_decode($this->request->getRawBody(), true);

        try{
            $this->_validaParametrosEntrada($arrParam);
        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $this->destinatario = $arrParam['destinatario'];
        $this->nota = $arrParam['nota'];

        $this->_criarDir();

        $this->salvarLog(__FUNCTION__ . ' Dados antes de enviar pra SEFAZ: '.print_r($this->nota,true));

        # Valida se a nota em questão está sendo emitida para o exterior:
        if(Formata::verificaCfopExterior($this->nota['cfop']) AND $this->destinatario['exterior']) {
            $this->notaExterior = true;
        }

        //seta os parametros que serão utilizados na geraçao do xml de entrada
        $this->_setParametrosCriacao();

        # Caso seja nota para o exterior, trata os dados obrigatórios:
        if($this->notaExterior) {
            $this->_tratarParametrosNotaExterior();
        }

        //valida as entradas iniciais. Se der erro já volta pro cliente
        $msg = $this->_validarEntradasIniciais();

        //se ja tiver alguma pendencia na entrada dos dados ja retorna com situaçao alerta
        if($msg) {
            
            $this->_setRetornoCliente();
            return $this->provide($this->retornoCliente);
        }

        $this->montarXml();

        try {

            $this->assinaXml();

        } catch(\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }
        
        try {

            $this->_enviarNfe();

        } catch(\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $retSefaz['codSefaz']  = $this->codSefaz;
        $retSefaz['strMotivo'] = $this->strMotivo;
        $msg = Formata::descMsgSefazPersonalizadas($retSefaz);

        $this->msg = $msg;

        $this->_setRetornoCliente();

        if(in_array($this->codSefaz, Formata::getStatusAutorizados())) {
            try {
                $this->_tryRecuperaArquivoXml();
            } catch(\Exception $e) {

                $this->_handleException($e);
                return $this->provide($this->retornoCliente);
            }
        }

        return $this->provide($this->retornoCliente);
    }

    /**
     * Verifica se o arquivo xml autorizado existe, caso contrário,
     * baixa o xml e adiciona o protocolo
     * @return bool
     * @throws \Exception
     */
    public function _tryRecuperaArquivoXml()
    {

        try {

            $arqXml = file_get_contents($this->_getXmlAprovado());

            $this->codSefaz = 0;
            if(empty($arqXml)) {

                $buscaPor = (!empty($this->chave)) ? 0 : 1;

                $this->_consultarNfe(true, $buscaPor);

                if($this->codSefaz == '100' OR $this->codSefaz == '135') {

                    return $this->adicionaProtocolo();
                } else {
                    throw new \Exception('Documento não autorizado na Receita! CodSefaz: '.$this->codSefaz);
                }
            }

        } catch(\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        return false;
    }

    /**
     * Monta o arquivo XML de envio
     */
    public function montarXml()
    {

        $nfe = new Make();

        $this->salvarLog('****NFe 4.0**** gerando chave com esses parametros: ' .
            $this->arrEnviar['cUF'] . ' - ' .
            $this->arrEnviar['ano'] .' - ' .
            $this->arrEnviar['mes'].' - ' .
            $this->arrEnviar['CNPJ'].' - ' .
            $this->arrEnviar['mod'].' - ' .
            $this->arrEnviar['serie'].' - ' .
            $this->arrEnviar['nNF'].' - ' .
            $this->arrEnviar['tpEmis'].' - ' .
            $this->arrEnviar['cNF']);

        $this->chave = Keys::build(
            $this->arrEnviar['cUF'],
            $this->arrEnviar['ano'],
            $this->arrEnviar['mes'],
            $this->arrEnviar['CNPJ'],
            $this->arrEnviar['mod'],
            $this->arrEnviar['serie'],
            $this->arrEnviar['nNF'],
            $this->arrEnviar['tpEmis'],
            $this->arrEnviar['cNF']
        );

        $this->salvarLog('****NFe 4.0**** criou a chave: '.$this->chave);
        $taginfNFe = new \stdClass();
        $taginfNFe->id     = $this->chave;
        $taginfNFe->versao = $this->arrEnviar['versao'];

        $this->salvarLog('****NFe 4.0**** vai chamar taginfNFe(): '.print_r($taginfNFe,true));
        $nfe->taginfNFe($taginfNFe);

        $cDV = substr($this->chave, -1);

        # Identificação da nota fiscal:
        $tagIde = new \stdClass();
        $tagIde->cDV      = $cDV;
        $tagIde->cUF      = $this->arrEnviar['cUF'];
        $tagIde->cNF      = $this->arrEnviar['cNF'];
        $tagIde->mod      = $this->arrEnviar['mod'];
        $tagIde->nNF      = $this->arrEnviar['nNF'];
        $tagIde->tpNF     = $this->arrEnviar['tpNF'];
        $tagIde->serie    = $this->arrEnviar['serie'];
        $tagIde->dhEmi    = $this->arrEnviar['dhEmi'];
        $tagIde->natOp    = $this->arrEnviar['natOp'];
        $tagIde->tpImp    = $this->arrEnviar['tpImp'];
        $tagIde->tpAmb    = $this->arrEnviar['tpAmb'];
        $tagIde->xJust    = $this->arrEnviar['xJust'];
        //$tagIde->infCpl   = $this->arrEnviar['infCpl'];
        $tagIde->idDest   = $this->arrEnviar['idDest'];
        $tagIde->cMunFG   = $this->arrEnviar['cMunFG'];
        $tagIde->tpEmis   = $this->arrEnviar['tpEmis'];
        $tagIde->finNFe   = $this->arrEnviar['finNFe'];
        $tagIde->dhCont   = $this->arrEnviar['dhCont'];
        $tagIde->indPres  = $this->arrEnviar['indPres'];
        $tagIde->procEmi  = $this->arrEnviar['procEmi'];
        $tagIde->verProc  = $this->arrEnviar['verProc'];
        $tagIde->dhSaiEnt = $this->arrEnviar['dhSaiEnt'];
        $tagIde->indFinal = $this->arrEnviar['indFinal'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagide(): '.print_r($tagIde,true));
        $nfe->tagide($tagIde);

        //Dados do emitente
        $tagEmit = new \stdClass();
        $tagEmit->IE    = $this->arrEnviar['IE'];
        $tagEmit->IM    = $this->arrEnviar['IM'];
        $tagEmit->cpf   = $this->arrEnviar['cpf'];
        $tagEmit->CRT   = $this->arrEnviar['CRT'];
        $tagEmit->CNPJ  = $this->arrEnviar['CNPJ'];
        $tagEmit->IEST  = $this->arrEnviar['IEST'];
        $tagEmit->CNAE  = $this->arrEnviar['CNAE'];
        $tagEmit->xNome = $this->arrEnviar['razaoSocial'];
        $tagEmit->xFant = $this->arrEnviar['nomeFantasia'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagemit(): '.print_r($tagEmit,true));
        $nfe->tagemit($tagEmit);

        if($this->arrEnviar['finNFe'] == FINALIDADE_DEVOLUCAO) {
            $tagRefNFe = new \stdClass();
            $tagRefNFe->refNFe = $this->arrEnviar['refNFe'];

            $nfe->tagrefNFe($tagRefNFe);
        }

        # Endereço do emitente:
        $tagEnderEmit = new \stdClass();
        $tagEnderEmit->xLgr    = $this->arrEnviar['xLgr'];
        $tagEnderEmit->nro     = $this->arrEnviar['nro'];
        $tagEnderEmit->xCpl    = $this->arrEnviar['xCpl'];
        $tagEnderEmit->xBairro = $this->arrEnviar['xBairro'];
        $tagEnderEmit->cMun    = $this->arrEnviar['cMun'];
        $tagEnderEmit->xMun    = $this->arrEnviar['xMun'];
        $tagEnderEmit->UF      = $this->arrEnviar['UF'];
        $tagEnderEmit->CEP     = $this->arrEnviar['CEP'];
        $tagEnderEmit->cPais   = $this->arrEnviar['cPais'];
        $tagEnderEmit->xPais   = $this->arrEnviar['xPais'];
        $tagEnderEmit->fone    = $this->arrEnviar['fone'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagenderEmit(): '.print_r($tagEnderEmit,true));
        $nfe->tagenderEmit($tagEnderEmit);

        # Dados destinatário:
        $tagDest = new \stdClass();
        $tagDest->xNome     = $this->arrEnviar['xNome'];
        $tagDest->indIEDest = $this->arrEnviar['indIEDest'];
        $tagDest->IE        = $this->arrEnviar['IEDest'];
        $tagDest->ISUF      = $this->arrEnviar['ISUFDest'];
        $tagDest->IM        = $this->arrEnviar['IMDest'];
        $tagDest->email     = $this->arrEnviar['emailDest'];
        $tagDest->CNPJ      = $this->arrEnviar['cnpjDest'];
        $tagDest->CPF       = $this->arrEnviar['cpfDest'];
        $tagDest->idEstrangeiro = $this->arrEnviar['idEstrangeiro'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagdest(): '.print_r($tagDest,true));
        $nfe->tagdest($tagDest);

        # Endereço do destinatário:
        $tagEnderDest = new \stdClass();
        $tagEnderDest->xLgr    = $this->arrEnviar['xLgrDest'];
        $tagEnderDest->nro     = $this->arrEnviar['nroDest'];
        $tagEnderDest->xCpl    = $this->arrEnviar['xCplDest'];
        $tagEnderDest->xBairro = $this->arrEnviar['xBairroDest'];
        $tagEnderDest->cMun    = $this->arrEnviar['cMunDest'];
        $tagEnderDest->xMun    = $this->arrEnviar['xMunDest'];
        $tagEnderDest->UF      = $this->arrEnviar['UFDest'];
        $tagEnderDest->CEP     = $this->arrEnviar['CEPDest'];
        $tagEnderDest->cPais   = $this->arrEnviar['cPaisDest'];
        $tagEnderDest->xPais   = $this->arrEnviar['xPaisDest'];
        $tagEnderDest->fone    = $this->arrEnviar['foneDest'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagenderDest(): '.print_r($tagEnderDest,true));
        $nfe->tagenderDest($tagEnderDest);

        foreach ($this->arrEnviar['aP'] as $prod)
        {

            # Dados principais do produto:
            $tagProd = new \stdClass();
            $tagProd->item     = $prod['nItem'];
            $tagProd->cProd    = $prod['cProd'];
            $tagProd->cEAN     = $prod['cEAN'];
            $tagProd->xProd    = $prod['xProd'];
            $tagProd->NCM      = $prod['NCM'];
            $tagProd->cBenef   = $prod['cBenef'];
            $tagProd->EXTIPI   = $prod['EXTIPI'];
            $tagProd->CFOP     = $prod['CFOP'];
            $tagProd->uCom     = $prod['uCom'];
            $tagProd->qCom     = $prod['qCom'];
            $tagProd->vUnCom   = $prod['vUnCom'];
            $tagProd->vProd    = $prod['vProd'];
            $tagProd->cEANTrib = $prod['cEANTrib'];
            $tagProd->uTrib    = $prod['uTrib'];
            $tagProd->qTrib    = $prod['qTrib'];
            $tagProd->vUnTrib  = $prod['vUnTrib'];
            $tagProd->vFrete   = $prod['vFrete'];
            $tagProd->vSeg     = $prod['vSeg'];
            $tagProd->vDesc    = $prod['vDesc'];
            $tagProd->vOutro   = $prod['vOutro'];
            $tagProd->indTot   = $prod['indTot'];
            $tagProd->xPed     = $prod['xPed'];
            $tagProd->nItemPed = $prod['nItemPed'];
            $tagProd->nFCI     = '';

            $this->salvarLog('****NFe 4.0**** vai chamar tagprod(): '.print_r($tagProd,true));
            $nfe->tagprod($tagProd);

            if(!empty($prod['CEST'])) {

                # Dados do CEST:
                $tagCEST = new \stdClass();
                $tagCEST->item      = $prod['nItem'];
                $tagCEST->CEST      = $prod['CEST'];
                $tagCEST->indEscala = $prod['indEscala'];
                $tagCEST->CNPJFab   = $prod['CNPJFab'];

                $this->salvarLog('****NFe 4.0**** vai chamar tagCEST(): '.print_r($tagCEST,true));
                $nfe->tagCEST($tagCEST);
            }

            if(!empty($prod['infAdProd'])) {

                $tagInfAdProd = new \stdClass();
                $tagInfAdProd->item      = $prod['nItem'];
                $tagInfAdProd->infAdProd = $prod['infAdProd'];

                $this->salvarLog('****NFe 4.0**** vai chamar taginfAdProd(): '.print_r($tagInfAdProd,true));
                $nfe->taginfAdProd($tagInfAdProd);
            }
        }

        foreach ($this->arrEnviar['aP'] as $prod)
        {
            $this->arrEnviar['vTotTrib'] = $prod['vTotTrib'] +  $this->arrEnviar['vTotTrib'];

            $tagImposto = new \stdClass();
            $tagImposto->item     = $prod['nItem'];
            $tagImposto->vTotTrib = $prod['vTotTrib'];

            $this->salvarLog('****NFe 4.0**** vai chamar tagimposto(): '.print_r($tagImposto,true));
            $nfe->tagimposto($tagImposto);
        }

        foreach ($this->arrEnviar['aP'] as $prod)
        {

            $modBCST  = (isset($prod['modBCST'])) ? $prod['modBCST'] : '0';
            $pRedBCST = ((isset($prod['pRedBCST']) AND !empty($prod['pRedBCST']))) ? $prod['pRedBCST'] : '';
            $vBCST    = (isset($prod['vBCST'])) ? Formata::toFloatPontoNota($prod['vBCST'],'',2) : '0.00';
            $pICMSST  = (isset($prod['pICMSST'])) ? $prod['pICMSST']: '0';
            $vICMSST  = (isset($prod['vICMSST'])) ? Formata::toFloatPontoNota($prod['vICMSST'],'',2) : '0';

            $prod['vBC'] = (isset($prod['vBC'])) ? Formata::toFloatPontoNota($prod['vBC'],'',2) : '0.00';

            if($prod['CST'] == '0') {
                $prod['CST'] = '00';
            }

            $tagICMS = new \stdClass();

            if($this->nota['finNFe'] == FINALIDADE_COMPLEMENTAR) {



                $tagICMS->item        = $prod['nItem'];
                $tagICMS->orig        = $prod['orig'];
                $tagICMS->CST         = $prod['CST'];
                $tagICMS->modBC       = $prod['modBC'];
                $tagICMS->vBC         = $prod['vBC'];
                $tagICMS->pICMS       = $prod['pICMS'];
                $tagICMS->vICMS       = $prod['vICMS'];
                $tagICMS->pFCP        = $prod['pFCP'];
                $tagICMS->vFCP        = $prod['vFCP'];
                $tagICMS->vBCFCP      = $prod['vBCFCP'];
                $tagICMS->modBCST     = $modBCST;
                $tagICMS->pMVAST      = $prod['pMVAST'];
                $tagICMS->pRedBCST    = $pRedBCST;
                $tagICMS->vBCST       = $vBCST;
                $tagICMS->pICMSST     = $pICMSST;
                $tagICMS->vICMSST     = $vICMSST;
                $tagICMS->vBCFCPST    = $prod['vBCFCPST'];
                $tagICMS->pFCPST      = $prod['pFCPST'];
                $tagICMS->vFCPST      = $prod['vFCPST'];
                $tagICMS->vICMSDeson  = '';
                $tagICMS->motDesICMS  = '';
                $tagICMS->pRedBC      = '';
                $tagICMS->vICMSOp     = '';
                $tagICMS->pDif        = '';
                $tagICMS->vICMSDif    = '';
                $tagICMS->vBCSTRet    = '';
                $tagICMS->pST         = '';
                $tagICMS->vICMSSTRet  = '';
                $tagICMS->vBCFCPSTRet = $prod['vBCFCPSTRet'];
                $tagICMS->pFCPSTRet   = $prod['pFCPSTRet'];
                $tagICMS->vFCPSTRet   = $prod['vFCPSTRet'];

            } else {

                $tagICMS->item        = $prod['nItem'];
                $tagICMS->orig        = $prod['orig'];
                $tagICMS->CST         = $prod['CST'];
                $tagICMS->modBC       = $prod['modBC'];
                $tagICMS->vBC         = $prod['vBC'];
                $tagICMS->pICMS       = $prod['pICMS'];
                $tagICMS->vICMS       = $prod['vICMS'];
                $tagICMS->pFCP        = $prod['pFCP'];
                $tagICMS->vFCP        = $prod['vFCP'];
                $tagICMS->vBCFCP      = $prod['vBCFCP'];
                $tagICMS->modBCST     = $modBCST;
                $tagICMS->pMVAST      = $prod['pMVAST'];
                $tagICMS->pRedBCST    = $pRedBCST;
                $tagICMS->vBCST       = $vBCST;
                $tagICMS->pICMSST     = $pICMSST;
                $tagICMS->vICMSST     = $vICMSST;
                $tagICMS->vBCFCPST    = $prod['vBCFCPST'];
                $tagICMS->pFCPST      = $prod['pFCPST'];
                $tagICMS->vFCPST      = $prod['vFCPST'];
                $tagICMS->vICMSDeson  = '';
                $tagICMS->motDesICMS  = '';
                $tagICMS->pRedBC      = $prod['pRedBC'];
                $tagICMS->vICMSOp     = $prod['vICMSOp'];
                $tagICMS->pDif        = $prod['pDif'];
                $tagICMS->vICMSDif    = $prod['vICMSDif'];
                $tagICMS->vBCSTRet    = $prod['vBCSTRet'];
                $tagICMS->pST         = $prod['pST'];
                $tagICMS->vICMSSTRet  = $prod['vICMSSTRet'];
                $tagICMS->vBCFCPSTRet = $prod['vBCFCPSTRet'];
                $tagICMS->pFCPSTRet   = $prod['pFCPSTRet'];
                $tagICMS->vFCPSTRet   = $prod['vFCPSTRet'];
            }

            $this->salvarLog('****NFe 4.0**** vai chamar tagICMS(): '.print_r($tagICMS,true));
            $nfe->tagICMS($tagICMS);

            if(isset($prod['cstIPI'])) {

                $prod['vBCIPI'] = (isset($prod['vBCIPI'])?Formata::toFloatPontoNota($prod['vBCIPI'],'',2): '0');
                $prod['vIPI'] = (isset($prod['vIPI'])?Formata::toFloatPontoNota($prod['vIPI'],'',2): '0.00');

                $this->salvarLog('****NFe 4.0**** vai gerar a tag IPI');

                # Lança os valores referentes ao IPI:
                $tagIPI = new \stdClass();
                $tagIPI->item = $prod['nItem'];

                if($this->arrEnviar['finNFe'] == FINALIDADE_DEVOLUCAO) {

                    $tagIPI->pDevol    = $prod['pDevol'];
                    $tagIPI->vIPIDevol = $prod['vIPIDevol'];

                    $this->salvarLog('****NFe 4.0**** vai chamar tagimpostoDevol(): '.print_r($tagIPI,true));
                    $nfe->tagimpostoDevol($tagIPI);

                } else {

                    $tagIPI->clEnq    = '';
                    $tagIPI->CNPJProd = '';
                    $tagIPI->cSelo    = '';
                    $tagIPI->qSelo    = '';
                    $tagIPI->cEnq     = $prod['cEnq'];
                    $tagIPI->CST      = $prod['cstIPI'];
                    $tagIPI->vIPI     = $prod['vIPI'];
                    $tagIPI->vBC      = $prod['vBCIPI'];
                    $tagIPI->pIPI     = $prod['pIPI'];
                    $tagIPI->qUnid    = '';
                    $tagIPI->vUnid    = '';

                    $this->salvarLog('****NFe 4.0**** vai chamar tagIPI(): '.print_r($tagIPI,true));
                    $nfe->tagIPI($tagIPI);
                }
            }

            if(isset($prod['cstPIS'])) {
                $tagPIS = new \stdClass();
                $tagPIS->item      = $prod['nItem'];
                $tagPIS->CST       = $prod['cstPIS'];
                $tagPIS->vBC       = $prod['vBCPIS'];
                $tagPIS->pPIS      = $prod['pPIS'];
                $tagPIS->vPIS      = $prod['vPIS'];
                $tagPIS->qBCProd   = $prod['qBCProd'];
                $tagPIS->vAliqProd = $prod['vAliqProd'];

                if($this->nota['finNFe'] == FINALIDADE_COMPLEMENTAR) {
                    $tagPIS->CST = $prod['cstPIS'] = '99';
                }

                $this->salvarLog('****NFe 4.0**** vai chamar tagPIS(): '.print_r($tagPIS,true));
                $nfe->tagPIS($tagPIS);
            }

            if(isset($prod['cstCOFINS'])) {

                $prod['vBCCOFINS'] = (isset($prod['vBCCOFINS'])?Formata::toFloatPontoNota($prod['vBCCOFINS'],'',2): '0.00');
                $prod['vCOFINS'] = (isset($prod['vCOFINS'])?Formata::toFloatPontoNota($prod['vCOFINS'],'',2): '0.00');

                $tagCOFINS = new \stdClass();
                $tagCOFINS->item      = $prod['nItem'];
                $tagCOFINS->CST       = $prod['cstCOFINS'];
                $tagCOFINS->vBC       = $prod['vBCCOFINS'];
                $tagCOFINS->pCOFINS   = $prod['pCOFINS'];
                $tagCOFINS->vCOFINS   = $prod['vCOFINS'];
                $tagCOFINS->qBCProd   = $prod['qBCProd'];
                $tagCOFINS->vAliqProd = $prod['vAliqProd'];

                if($this->nota['finNFe'] == FINALIDADE_COMPLEMENTAR) {
                    $tagCOFINS->CST = $prod['cstCOFINS'] = '99';
                }

                $this->salvarLog('****NFe 4.0**** vai chamar tagCOFINS(): '.print_r($tagCOFINS,true));
                $nfe->tagCOFINS($tagCOFINS);
            }

            if($this->ehForaEstado) {

                if($this->destinatario['tipContribuinte'] == TIP_CONTRIBUINTE_CONSUMIDOR) {

                    $tagICMSUFDest = new \stdClass();
                    $tagICMSUFDest->item           = $prod['nItem'];
                    $tagICMSUFDest->vBCUFDest      = $prod['vBCUFDest'];
                    $tagICMSUFDest->vBCFCPUFDest   = $prod['vBCFCPUFDest'];
                    $tagICMSUFDest->pFCPUFDest     = $prod['pFCPUFDest'];
                    $tagICMSUFDest->pICMSUFDest    = $prod['pICMSUFDest'];
                    $tagICMSUFDest->pICMSInter     = $prod['pICMSInter'];
                    $tagICMSUFDest->pICMSInterPart = $prod['pICMSInterPart'];
                    $tagICMSUFDest->vFCPUFDest     = $prod['vFCPUFDest'];
                    $tagICMSUFDest->vICMSUFDest    = $prod['vICMSUFDest'];
                    $tagICMSUFDest->vICMSUFRemet   = $prod['vICMSUFRemet'];

                    $this->salvarLog('****NFe 4.0**** vai chamar tagICMSUFDest(): '.print_r($tagICMSUFDest,true));
                    $nfe->tagICMSUFDest($tagICMSUFDest);
                }
            }
        }

        $tagICMSTot = new \stdClass();
        $tagICMSTot->vBC          = $this->arrEnviar['vTotalBCICMS'];
        $tagICMSTot->vICMS        = $this->arrEnviar['vICMSTotal'];
        $tagICMSTot->vICMSDeson   = $this->arrEnviar['vICMSDesonTotal'];
        $tagICMSTot->vBCST        = $this->arrEnviar['vBCST'];
        $tagICMSTot->vST          = $this->arrEnviar['vST'];
        $tagICMSTot->vProd        = $this->arrEnviar['vProd'];
        $tagICMSTot->vFrete       = $this->arrEnviar['vFrete'];
        $tagICMSTot->vSeg         = $this->arrEnviar['vSeg'];
        $tagICMSTot->vDesc        = $this->arrEnviar['vDesc'];
        $tagICMSTot->vII          = $this->arrEnviar['vIITotal'];
        $tagICMSTot->vIPI         = $this->arrEnviar['vIPITotal'];
        $tagICMSTot->vPIS         = $this->arrEnviar['vPISTotal'];
        $tagICMSTot->vCOFINS      = $this->arrEnviar['vCOFINSTotal'];
        $tagICMSTot->vOutro       = $this->arrEnviar['vOutro'];
        $tagICMSTot->vNF          = $this->arrEnviar['vNF'];
        $tagICMSTot->vIPIDevol    = $this->arrEnviar['vIPIDevolTotal'];
        $tagICMSTot->vTotTrib     = Formata::toFloatPontoNota($this->arrEnviar['vTotTrib']);
        $tagICMSTot->vFCP         = $this->arrEnviar['vFCPTotal'];
        $tagICMSTot->vFCPST       = $this->arrEnviar['vFCPSTTotal'];
        $tagICMSTot->vFCPSTRet    = $this->arrEnviar['vFCPSTRetTotal'];
        $tagICMSTot->vFCPUFDest   = $this->arrEnviar['vFCPUFDestTotal'];
        $tagICMSTot->vICMSUFDest  = $this->arrEnviar['vICMSUFDestTotal'];
        $tagICMSTot->vICMSUFRemet = $this->arrEnviar['vICMSUFRemetTotal'];

        $this->salvarLog('****NFe 4.0**** vai chamar tagICMSTot(): '.print_r($tagICMSTot,true));
        $nfe->tagICMSTot($tagICMSTot);

        if($this->arrEnviar['vOrig'] > 0) {

            # Nâo adiciona a tag com as fatura em caso de outras saídas:
            if($this->nota['codNatOp'] != NATUREZA_OUTRAS_SAIDAS) {

                $tagFat = new \stdClass();
                $tagFat->nFat  = $this->arrEnviar['nFat'];
                $tagFat->vOrig = $this->arrEnviar['vOrig'];
                $tagFat->vDesc = $this->arrEnviar['vDescFat'];
                $tagFat->vLiq  = $this->arrEnviar['vLiq'];

                $this->salvarLog('****NFe 4.0**** vai chamar tagfat(): '.print_r($tagFat,true));
                $nfe->tagfat($tagFat);
            }

            $tagPag = new \stdClass();
            $tagPag->vTroco = '';

            $this->salvarLog('****NFe 4.0**** vai chamar tagpag(): '.print_r($tagPag,true));
            $nfe->tagpag($tagPag);

            $tagDetPag = new \stdClass();
            $tagDetPag->tPag      = $this->arrEnviar['tPag'];
            $tagDetPag->vPag      = $this->arrEnviar['vPag'];
            $tagDetPag->CNPJ      = $this->arrEnviar['CNPJCredenciadora'];
            $tagDetPag->tBand     = $this->arrEnviar['tBand'];
            $tagDetPag->cAut      = $this->arrEnviar['cAut'];
            $tagDetPag->tpIntegra = $this->arrEnviar['tpIntegra'];

            $this->salvarLog('****NFe 4.0**** vai chamar tagdetPag(): '.print_r($tagDetPag,true));
            $nfe->tagdetPag($tagDetPag);

            if(isset($this->arrEnviar['parcela']) AND $this->nota['avAp'] == PARC_APRAZO) {

                foreach($this->arrEnviar['parcela'] as $k => $parcela)
                {

                    $tagDup = new \stdClass();
                    $tagDup->nDup  = $parcela['nDup'];
                    $tagDup->dVenc = $parcela['dVenc'];
                    $tagDup->vDup  = $parcela['vDup'];

                    $this->salvarLog('****NFe 4.0**** vai chamar tagdup(): '.print_r($tagDup,true));
                    $nfe->tagdup($tagDup);
                }
            }
        }

        $this->salvarLog('****NFe 4.0**** vai tagtransp tagdup(): '.print_r(['modFrete' => $this->arrEnviar['modFrete']],true));
        $nfe->tagtransp((object) ['modFrete' => $this->arrEnviar['modFrete']]);

        # Minas gerais regeita transportador e veiculo para operações interestaduais:
        if($this->arrEnviar['modFrete'] != 9) {

            $this->salvarLog('****NFe 4.0**** vai colocar as tags de transportadora');

            if(((isset($this->arrEnviar['numCNPJTransp']) AND !empty($this->arrEnviar['numCNPJTransp'])) OR (isset($this->arrEnviar['numCPFTransp']) AND !empty($this->arrEnviar['numCPFTransp']))) AND !$this->ehForaEstado) {

                $tagTransporta = new \stdClass();
                $tagTransporta->xNome  = $this->arrEnviar['xNomeTransp'];
                $tagTransporta->IE     = $this->arrEnviar['numIETransp'];
                $tagTransporta->xEnder = $this->arrEnviar['xEnderTransp'];
                $tagTransporta->xMun   = $this->arrEnviar['xMunTransp'];
                $tagTransporta->UF     = $this->arrEnviar['siglaUF'];
                $tagTransporta->CNPJ   = $this->arrEnviar['numCNPJTransp'];
                $tagTransporta->CPF    = $this->arrEnviar['numCPFTransp'];

                $this->salvarLog('****NFe 4.0**** vai tagtransp tagtransporta(): '.print_r($tagTransporta,true));
                $nfe->tagtransporta($tagTransporta);
            }

            if(!empty($this->arrEnviar['placaVeic']) AND $this->arrEnviar['modFrete'] != 3 AND $this->arrEnviar['modFrete'] != 4 AND !$this->ehForaEstado) {

                $tagVeicTransp = new \stdClass();
                $tagVeicTransp->placa = $this->arrEnviar['placaVeic'];
                $tagVeicTransp->UF    = $this->arrEnviar['siglaUFVeic'];
                $tagVeicTransp->RNTC  = $this->arrEnviar['rntc'];

                $this->salvarLog('****NFe 4.0**** vai tagtransp tagveicTransp(): '.print_r($tagVeicTransp,true));
                $nfe->tagveicTransp($tagVeicTransp);
            }

            $tagVol = new \stdClass();
            $tagVol->item  = '';
            $tagVol->qVol  = $this->arrEnviar['qVolTransp'];
            $tagVol->esp   = $this->arrEnviar['espTransp'];
            $tagVol->marca = $this->arrEnviar['marcaTransp'];
            $tagVol->nVol  = $this->arrEnviar['nVolTransp'];
            $tagVol->pesoL = $this->arrEnviar['pesoLTransp'];
            $tagVol->pesoB = $this->arrEnviar['pesoBTransp'];

            $this->salvarLog('****NFe 4.0**** vai tagtransp tagvol(): '.print_r($tagVol,true));
            $nfe->tagvol($tagVol);
        }

        $tagInfAdic = new \stdClass();
        $tagInfAdic->infAdFisco = $this->arrEnviar['infAdFisco'];
        $tagInfAdic->infCpl     = $this->arrEnviar['infCpl'];

        $this->salvarLog('****NFe 4.0**** vai tagtransp taginfAdic(): '.print_r($tagInfAdic,true));
        $nfe->taginfAdic($tagInfAdic);

        if(isset($this->arrEnviar['exporta'])) {

            $tagExporta = new \stdClass();
            $tagExporta->UFSaidaPais  = $this->arrEnviar['exporta']['UFEmbarq'];
            $tagExporta->xLocExporta  = $this->arrEnviar['exporta']['xLocEmbarq'];
            $tagExporta->xLocDespacho = '';

            $this->salvarLog('****NFe 4.0**** vai tagtransp tagexporta(): '.print_r($tagExporta,true));
            $nfe->tagexporta($tagExporta);
        }

        if ($nfe->montaNFe()) {

            header('Content-type: text/xml; charset=UTF-8');

            $xml      = $nfe->getXML();
            $filename = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente']."/entradas/$this->chave-nfe.xml";

            $this->salvarLog('****NFe 4.0**** Montou XML : '.print_r($xml,true));
            $this->salvarLog('****NFe 4.0**** Vai gravar XML : '.$filename);

            file_put_contents($filename, $xml);
            chmod($filename, 0777);
        } else {

            foreach ($nfe->erros as $err)
            {
                $this->salvarLog(['desc' => 'erro ao gerar o xml da nota: '.@$err['tag'].' ---- '.@$err['desc']]);
            }
        }
    }

    /**
     * Trata alguns dados referentes a nota de exportação
     */
    protected function _tratarParametrosNotaExterior()
    {

        $this->arrEnviar['cMunDest']  = '9999999';
        $this->arrEnviar['xMunDest']  = 'EXTERIOR';
        $this->arrEnviar['UFDest']    = 'EX';
        $this->arrEnviar['CEPDest']   = '00000000';
        $this->arrEnviar['cPaisDest'] = $this->destinatario['cPaisDest'];
        $this->arrEnviar['xPaisDest'] = Formata::truncate(Formata::trocarCaracteresEsp(strtoupper($this->paisDestino['xPaisDest'])),60);

        # Caso seja uma exportação, informa os dados da tag exporta:
        if(!Formata::verificaCfopImportacao($this->nota['cfop'])) {

            $this->arrEnviar['exporta'] = [

                'UFEmbarq'   => (isset($this->arrEnviar['siglaUF'])    ? $this->arrEnviar['siglaUF']    : $this->arrEnviar['UF']),
                'xLocEmbarq' => (isset($this->arrEnviar['xMunTransp']) ? $this->arrEnviar['xMunTransp'] : $this->arrEnviar['xMun'])
            ];
        }
    }

    /**
     * Atualiza as informaços da nfe de uma empresa e envia o arquivo de certificado
     */
    public function atualizarDadosNfeEmpresa()
    {

        $this->salvarLog('****NFe 4.0**** post:'.print_r($_POST,true));
        $this->salvarLog('****NFe 4.0**** ff:'.print_r($_FILES,true));

        $arrParam = (isset($_POST['data']))?json_decode($_POST['data'], true):$_POST;

        if(empty($arrParam['emp_nfe_pwd']) AND !isset($_FILES['file'])) {
            $retorno = Formulario::getDefaultMessages(ERRO,ATUALIZAR,['Nenhuma ação a ser realizada.']);

            return $this->provide($retorno);
        }

        $this->_criarDir();

        $errorUpload = false;
        $arrAtualizar = [];

        //upload do arquivo
        if(isset($_FILES['file'])) {

            if (preg_match('/\.(pfx|PFX){1}$/i', $_FILES['file']['name'])) {

                $this->salvarLog('****NFe 4.0**** vai:' . $this->_getDirNfe() . '/certs/');
                $ret = Formulario::upload_arquivo($_FILES['file'], $this->_getDirNfe() . '/certs/');
                //chmod($this->_getDirNfe() . '/certs/', 0777);

                $this->salvarLog('****NFe 4.0**** nome do novo arquivo: ' . $ret);

                $arrAtualizar['emp_nfe_cert_arq'] = $ret;

                $errorUpload = (!empty($ret) ? false : true);
            } else {
                
            }
        }

        if(!empty($arrParam['emp_nfe_pwd'])) {

            $arrAtualizar['emp_nfe_pwd'] = $arrParam['emp_nfe_pwd'];
        }

        $arrWhere['filtro']['emp_cod_emp'] = $this->decodedJwt['usu_cod_emp'];
        $arrWhere['filtro']['emp_ident_emp'] = $this->identEmp;

        $this->salvarLog('****NFe 4.0**** where:'.print_r($arrWhere,true));

        $objSisEmp = SisEmp::findFirst($arrWhere);

        $objSisEmp->token = ((isset($this->decodedJwt['token']))?$this->decodedJwt['token']:'');
        
        if($objSisEmp->save($arrAtualizar) AND !$errorUpload) {

            $retorno = Formulario::getDefaultMessages(SUCESSO,ATUALIZAR);
            $this->atualizaDataValidadeCertificado();

        } else {

            $retorno = Formulario::getDefaultMessages(ERRO,ATUALIZAR,['Houve algum erro ao atualizar ou erro ao subir o arquivo']);
        }

        return $this->provide($retorno);

    }


    /**
     * seta as variaveis de retorno para o cliente
     */
    protected function _setRetornoCliente()
    {
        $arr['codSefaz'] = $this->codSefaz;
        $arr['strMotivo'] = $this->strMotivo;
        $arr['msg'] = $this->msg;
        $arr['error'] = $this->error;
        $arr['chave'] = $this->chave;

        if(isset($this->nroRecibo) AND !empty($this->nroRecibo)) {
            $arr['recibo'] = $this->nroRecibo;
        }

        $this->retornoCliente = $arr;
    }

    /**
     * Obtem os dados de algum cfop
     */
    protected function _getVixCfo($id)
    {
        $arrFiltroCfop['filtro']['cfo_seq'] = $id;

        $vixCfo = [];
        $objVixCfo = VixCfo::findFirst($arrFiltroCfop);
        if($objVixCfo) {
            $vixCfo = $objVixCfo->toArray();
        }
        return $vixCfo;
    }


    /**
     * Assina o XML da NFE
     */
    function assinaXml()
    {

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());

        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $nfe->model('55');

        $this->salvarLog('****NFe 4.0**** entra em assinaXml');
        $filename = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/entradas/$this->chave-nfe.xml";
        $xml = file_get_contents($filename);

        try {

            $xml = $nfe->signNFe($xml);

        } catch(\Exception $e){

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $filename = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/assinadas/$this->chave-nfe.xml";
        $this->salvarLog('****NFe 4.0**** Assinou XML: ' . print_r($xml,true));
        $this->salvarLog('****NFe 4.0**** Vai gragar XML assinado: ' . $filename);
        file_put_contents($filename, $xml);
        chmod($filename, 0777);
    }

    /**
     * Valida o XML da NFE
     */
    function validaXml()
    {

        $this->salvarLog('****NFe 4.0**** entra validaxml');

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');

        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $xml = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente']."/assinadas/$this->chave-nfe.xml";
        if(file_exists($xml)){
            $xml = file_get_contents($xml);
        } else {
            throw new \Exception('O XML ainda não foi assinado!');
        }

        try {

            $nfe->sefazValidate($xml);

        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $this->salvarLog(['desc' => 'Erro em '.__METHOD__.' arq xml: ' .$xml .' Erro: '.print_r($nfe->errors,true)]);

        if(isset($nfe->errors[0][1])) {
            $msg = $nfe->errors[0][1];
        } else if(isset($nfe->errors[0][0])) {
            $msg = $nfe->errors[0][0];
        } else {
            $msg = '';
        }
        $retorno = Formulario::getDefaultMessages(ERRO,INSERIR,[$msg]);

        $msg = 'Algum erro no XML de envio. Verifique novamente os dados de entrada. Detalhe: ' .$retorno['msg'];
        $this->strMotivo = $msg;
        $this->codSefaz = 225;
        $this->error = true;
        $this->msg = $msg;

        $this->salvarLog('****NFe 4.0**** 222 erro em validar xml: ' . print_r($retorno,true));

        $this->_setRetornoCliente();

        throw new \Exception($this->strMotivo);
    }

    /**
     * Envia o lote da NFE
     */
    function _enviarNfe()
    {

        $this->salvarLog('****NFe 4.0**** entra em _enviarNfe');

        $aResposta = [];

        $aXml[]  = file_get_contents($this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/assinadas/$this->chave-nfe.xml");
        $idLote  = 0;
        $indSinc = '0';
        $flagZip = false;

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');

        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $this->salvarLog('****NFe 4.0**** llll:'.$this->_getDirNfe().'/'.$this->empresa['pasta_ambiente']."/assinadas/$this->chave-nfe.xml");

        try {

            $retorno = $nfe->sefazEnviaLote($aXml, $idLote, $indSinc, $flagZip);
            
        } catch(\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $this->salvarLog(['desc' => '$aResposta enviarNfe: ' .print_r($aResposta,true)]);

        $stdCl = new Standardize($retorno);
        //nesse caso o $arr irá conter uma representação em array do XML retornado
        $aResposta = $stdCl->toArray();

        if($aResposta['cStat'] == '103'){

            $this->salvarLog('****NFe 4.0**** vai consultar NFEE');

            $this->nroRecibo = $aResposta['infRec']['nRec'];
            $this->_consultarNfe(false, 1);

        } else if($aResposta['cStat'] == '105'){

            $this->nroRecibo = $aResposta['nRec'];
//            sleep(5);
//            $this->_consultarNfe(false, 1);

        } else {

            $this->codSefaz  = $aResposta['cStat'];
            $this->strMotivo = $aResposta['xMotivo'];
        }

        $retornoNfe['codSefaz']  = $this->codSefaz;
        $retornoNfe['strMotivo'] = $this->strMotivo;

        $this->salvarLog('****NFe 4.0**** nesse momento esta com status sefaz: '.$this->codSefaz);

        $msg = Formata::descMsgSefazPersonalizadas($retornoNfe);
    }


    /**
     * Consulta um determinado recibo
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    function consultarNfe()
    {
        $arrParam = $this->searchFields;
        $this->salvarLog('****NFe 4.0**** inicio metodo vai consultar a nfe:'.print_r($arrParam,true));

        $this->nroRecibo = (!empty($arrParam['recibo'])) ? $arrParam['recibo'] : '';
        $this->chave     = (!empty($arrParam['chave']))  ? $arrParam['chave']  : '';

        if((!isset($this->nroRecibo) OR empty($this->nroRecibo)) AND (!isset($this->chave) OR empty($this->chave))) {

            $retorno = Formulario::getDefaultMessages(ERRO, ATUALIZAR, ['Nro do Recibo ou chave não fornecidos']);
            return $this->provide($retorno);
        }

        $apenasConsulta = false;
        if(isset($arrParam['apenasconsulta'])) {
            $apenasConsulta = ($arrParam['apenasconsulta'] == 0);
        }

        try {

            $buscaPor = (!empty($this->chave)) ? 2 : 1;
            $this->_consultarNfe($apenasConsulta, $buscaPor);

        } catch(\Exception $e){

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $retorno['ok'] = true;
        $retorno['codSefaz']  = $this->codSefaz;
        $retorno['strMotivo'] = $this->strMotivo;
        $retorno['aResposta'] = $this->aResposta;

        return $this->provide($retorno);
    }


    /**
     * Consulta a nfe por chave ou recibo
     * @param bool $apenasConsulta
     * @param int $buscaPor
     * @throws \Exception
     */
    protected function _consultarNfe($apenasConsulta = false,$buscaPor=1,$cancelamento=false)
    {

        $this->salvarLog('****NFe 4.0**** entra pra consultar');

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');

            $this->salvarLog('****NFe 4.0**** vai buscar por : ' . $this->nroRecibo);
            $retorno = ($buscaPor == 1)
                ? $nfe->sefazConsultaRecibo($this->nroRecibo, $this->empresa['tpAmb'])
                : $nfe->sefazConsultaChave($this->chave, $this->empresa['tpAmb']);

        } catch (\Exception $e) {

            $this->_handleException($e);
            throw new \Exception($e->getMessage());
        }

        $stdCl = new Standardize($retorno);
        //nesse caso o $arr irá conter uma representação em array do XML retornado
        $arr = $stdCl->toArray();

        $aResposta = $arr;

        if(isset($arr['protNFe']['infProt'])) {
            $aResposta = $arr['protNFe']['infProt'];
        }

        if(isset($arr['procEventoNFe']['evento']['infEvento'])) {
            $aResposta = $arr['procEventoNFe']['evento']['infEvento'];
        }

        if(isset($arr['procEventoNFe']['retEvento']['infEvento'])) {
            $aResposta = $arr['procEventoNFe']['retEvento']['infEvento'];
        }

        $arrStatusCancelados = Formata::getStatusCancelados();

        if(in_array($arr['cStat'],$arrStatusCancelados) ) {
            $aResposta = $arr;
        }

        //$this->salvarLog(['desc' => htmlspecialchars($nfe->soapDebug)]);
        $this->salvarLog(['desc' => '$aResposta consulta nfe: '. print_r($aResposta,true)]);
        $this->salvarLog(['desc' => '$retorno consulta nfe: '. print_r($retorno,true)]);

        $this->aResposta      = $aResposta;
        $arrStatusAutorizados = Formata::getStatusAutorizados();

        if(!$apenasConsulta AND in_array($aResposta['cStat'],$arrStatusAutorizados) || ($aResposta['cStat'] == 104 AND isset($aResposta['aProt'][0]['cStat']) AND in_array($aResposta['aProt'][0]['cStat'],$arrStatusAutorizados))) {

            $dir = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/temporarias/20" . substr($this->chave,2,4);
            $this->_checkDirExists($dir);

            file_put_contents("$dir/$this->nroRecibo-retConsReciNFe.xml", $retorno);
            chmod("$dir/$this->nroRecibo-retConsReciNFe.xml", 0777);

            $this->adicionaProtocolo();
        }

        # Caso seja a consulta de um cancelamento, grava o XML:
//        if(!$apenasConsulta AND $cancelamento) {
//
//            $arrStatusCancelados = Formata::getStatusCancelados();
//
//            if(in_array($aResposta['cStat'],$arrStatusCancelados) ) {
//
//                $dir = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/canceladas/20" . substr($this->chave,2,4);
//                $this->_checkDirExists($dir);
//                file_put_contents("$dir/$this->chave-CancNFe-procEvento.xml", $retorno);
//                chmod("$dir/$this->chave-CancNFe-procEvento.xml", 0777);
//            }
//        }

        if(isset($aResposta['aProt'][0]['cStat'])) {

            $this->codSefaz  = $aResposta['aProt'][0]['cStat'];
            $this->strMotivo = $aResposta['aProt'][0]['xMotivo'];

        } else if(isset($aResposta['evento'][0]['cStat'])) {

            $this->codSefaz  = $aResposta['evento'][0]['cStat'];
            $this->strMotivo = $aResposta['evento'][0]['xMotivo'];

        } else if(isset($aResposta['aEvent'][0][0]['cStat']))  {

            $this->codSefaz  = $aResposta['aEvent'][0][0]['cStat'];
            $this->strMotivo = $aResposta['aEvent'][0][0]['xMotivo'];

        } else {

            $this->codSefaz  = $aResposta['cStat'];
            $this->strMotivo = $aResposta['xMotivo'];

            if(empty($this->strMotivo)) {
                $this->strMotivo = 'A receita não retornou nenhuma informação adicional!';
            }

            $this->strMotivo .= (isset($aResposta['xMsg'])) ? " {$aResposta['xMsg']}" : '';
        }

        if($this->codSefaz == '105'){

            $this->salvarLog(' ### RETORNOU 105 VAI CONSULTAR EM 5 SEGUNDOS ### ');

            $this->nroRecibo = $aResposta['nRec'];
            sleep(5);
            $this->_consultarNfe(false, 1);
        }
    }

    /**
     * Adiciona o protocolo da NFE
     */
    function adicionaProtocolo()
    {
        
        $pathNFefile  = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente']."/assinadas/$this->chave-nfe.xml";
        $pathProtfile = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/temporarias/'. '20' . substr($this->chave,2,4) .'/'.$this->nroRecibo.'-retConsReciNFe.xml';

        if(!file_exists($pathProtfile)) {
            $pathProtfile = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/temporarias/'.date('Ym').'/'.$this->nroRecibo.'-retConsReciNFe.xml';
        }

        $xml  = file_get_contents($pathNFefile);
        $prot = file_get_contents($pathProtfile);

        $protocolo = new Protocol();
        $retProt   = $protocolo->add($xml,$prot);

        $dir = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/enviadas/aprovadas/20'. substr($this->chave,2,4);

        $this->_checkDirExists($dir);

        if(file_put_contents("$dir/$this->chave-protNFe.xml", $retProt)) {
            chmod("$dir/$this->chave-protNFe.xml", 0777);
            return true;
        }

        return false;
    }


    /**
     * Imprime o DANFE
     */
    public function danfe($ev = 'autorizado')
    {

        ini_set('memory_limit','512M');
        ob_start();

        $this->chave = $this->searchFields['chave'];

        if(isset($ev)) {

            switch($ev) {

                case 'autorizado':

                    $this->salvarLog('****NFe 4.0**** ta autorizado vai tentar gerar danfe: ' . $this->_getDirNfe().'/danfe/'.$this->searchFields['chave'].'.pdf');
                    $pdf = $this->_gerarDanfe();

                    header('Content-Type: application/pdf');
                    echo $pdf;

                    break;

                case 'cancelado':
                    $id = $this->_gerarDanfeEvento('cancelado');
                    $this->daevento->printDocument($id.'.pdf', 'I');
                    break;

                case 'cce':
                    $this->seqEvento = $this->searchFields['seq'];
                    $id = $this->_gerarDanfeEvento('cce');
                    $this->daevento->printDocument($id.'.pdf', 'I');
                    break;
            }
        }

        exit;
    }

    /**
     * Retorna o endereço do arquivo autorizado, de uma determinada finNroLan e chave
     * @return string
     */
    protected function _getXmlAprovado()
    {
        $anoMes = '20'.substr($this->chave,2,4);
        return $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/enviadas/aprovadas/$anoMes/$this->chave-protNFe.xml";
    }

    /**
     * Retorna o xml de entrada
     * @return string
     */
    protected function _getXmlEntrada()
    {

        $this->salvarLog('****NFe 4.0**** gerar: '.$this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/entradas/'.$this->chave.'-nfe.xml');
        $arqXmlEntrada = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/entradas/'.$this->chave.'-nfe.xml';

        return $arqXmlEntrada;
    }

    /**
     * Retorna o endereço do arquivo cancelado, de uma determinada finNroLan e chave
     * @return string
     */
    protected function _getXmlCancelado()
    {
        $anoMes = '20'.substr($this->chave,2,4);
        return $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/canceladas/'.$anoMes.'/'.$this->chave.'-CancNFe-procEvento.xml';
    }


    /**
     * Retorna o endereço do arquivo cce, de uma determinada finNroLan e chave
     * @return string
     */
    protected function _getXmlCce()
    {

        $anoMes = '20'.substr($this->chave,2,4);

        $arqXmlCce = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/cartacorrecao/'.$anoMes.'/'.$this->chave.'-CCe-'.$this->seqEvento.'-procEvento.xml';

        if(!file_exists($arqXmlCce)) {
            $arqXmlCce = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/cartacorrecao/'.$anoMes.'/'.$this->chave.'-CCe-procEvento.xml';

            if(!file_exists($arqXmlCce)) {
                $ano = substr($anoMes,0,4);
                $mes = substr($anoMes,4,2);
                $mes = Formata::preencherStr($mes,2,0);

                $arqXmlCce = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/cartacorrecao/'.$ano.$mes.'/'.$this->chave.'-CCe-procEvento.xml';

                if(!file_exists($arqXmlCce)) {
                    $arqXmlCce = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/cartacorrecao/'.$ano.$mes.'/'.$this->chave.'-CCe-'.$this->seqEvento.'-procEvento.xml';

                }
            }
        }

        $this->salvarLog('****NFe 4.0**** xml cce: ' .$arqXmlCce);

        return $arqXmlCce;
    }




    /**
     * Gera a Danfe de um evento (cancelado, )
     * @param $evento
     */
    public function _gerarDanfeEvento($evento)
    {

        $aEnd = array(
            'razao'       => $this->empresa['razaoSocialEmitente'],
            'logradouro'  => $this->empresa['xLgrEmitente'],
            'numero'      => $this->empresa['nroEmitente'],
            'complemento' => $this->empresa['xCplEmitente'],
            'bairro'      => $this->empresa['xBairroEmitente'],
            'CEP'         => $this->empresa['CEPEmitente'],
            'municipio'   => $this->empresa['xMunEmitente'],
            'UF'          => $this->empresa['UFEmitente'],
            'telefone'    => $this->empresa['foneEmitente'],
            'email'       => $this->empresa['emailEmitente']
        );

        if($evento == 'cancelado') {

            $this->arqXmlDanfe = $this->_getXmlCancelado();

            if(file_exists($this->arqXmlDanfe)) {

                $xml = $this->arqXmlDanfe;

                $docxml = FilesFolders::readFile($xml);

                $this->salvarLog('****NFe 4.0**** $docxml: '. print_r($docxml,true));

                $this->daevento = new Daevento($docxml, 'P', 'A4', LOGO_EMPRESA, 'I', '', '',$aEnd);

                $id = $this->daevento->chNFe . '';
                $this->salvarLog('****NFe 4.0**** idd vale: '. $id);

                return $id;

            } else {
                die('Arquivo DANFE ainda não foi gerado. A nota está cancelada?');
            }
        } else if($evento == 'cce') {

            $this->arqXmlDanfe = $this->_getXmlCce();

            if(file_exists($this->arqXmlDanfe)) {

                $xml = $this->arqXmlDanfe;

            } else {
                if($this->empresa['emp_tip_nota'] == TIPO_NOTA_A1 || empty($this->empresa['emp_tip_nota'])) {
                    try {
                        if($this->_tryRecuperaArquivoXmlCCe()) {
                            $xml = $this->_getXmlCce();
                        } else {
                            die('Arquivo XML CCe ainda não foi gerado. Foi emitida alguma carta de correção?');
                        }
                    } catch(\Exception $e) {
                        $this->_handleException($e);
                        die($e->getMessage());
                    }
                } else {
                    die('Arquivo XML CCe ainda não foi gerado. Foi emitida alguma carta de correção?');
                }
            }

            $docxml = FilesFolders::readFile($xml);

            $this->salvarLog('****NFe 4.0**** $docxml: '. print_r($docxml,true));

            $this->daevento = new Daevento($docxml, 'P', 'A4', LOGO_EMPRESA, 'I', '', '',$aEnd);

            $id = $this->daevento->chNFe;

            $this->salvarLog('****NFe 4.0**** idd vale: '. $id);

            return $id;
        }
    }

    /**
     * Gera a Danfe
     * @param $finNroLan
     * @param $chave
     * @return string
     */
    protected function _gerarDanfe()
    {

        $this->arqXmlDanfe = ($this->preview) ? $this->_getXmlEntrada() : $this->_getXmlAprovado();
        
        if(file_exists($this->arqXmlDanfe)) {

            $xml = $this->arqXmlDanfe;

        } else {

            die('Arquivo DANFE ainda não foi gerado. A nota está autorizada?');
        }

        $this->salvarLog(['desc' => 'vai gerar a danfe desse arquivo: '.$xml]);

        $docxml = FilesFolders::readFile($xml);

        $docxml = str_replace('&amp;amp;','&amp;',$docxml);

        $this->objDanfe = new Danfe($docxml, 'P', 'A4', LOGO_EMPRESA, 'I', '');

        $this->objDanfe->montaDANFE();
        return $this->objDanfe->render();
    }

    /**
     * Validar a ação de cancelamento da nota
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    protected function _validarCancelar($arrParam)
    {

        if(!isset($arrParam['chave']) OR empty($arrParam['chave'])) {
            throw new \Exception('Faltou informar a Chave da nota a ser Cancelada');
        }

        if(!isset($arrParam['justificativa'])) {
            throw new \Exception('Faltou informar a Justificativa do Cancelamento!');
        }

        if(strlen($arrParam['justificativa']) < 15) {
            throw new \Exception('A Justificativa informada possui menos de 15 caracteres!');
        }
    }

    /**
     * Cancelar uma NFE
     */
    public function cancelarNfe()
    {

        $this->salvarLog('****NFe 4.0**** cliente chamou '.__METHOD__);

        $arrParam = json_decode($this->request->getRawBody(), true);

        try {
            $this->_validarCancelar($arrParam);
        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $this->chave = $arrParam['chave'];

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');

            $retorno = $nfe->sefazConsultaChave($this->chave, $this->empresa['tpAmb']);

        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $stdCl = new Standardize($retorno);
        //nesse caso o $arr irá conter uma representação em array do XML retornado
        $this->aResposta = $stdCl->toArray();

        $this->salvarLog('****NFe 4.0**** voltando pro metodo cancelar com a resposta: ' . print_r($this->aResposta, true));

        if (isset($this->aResposta['protNFe'])) {

            $this->salvarLog('****NFe 4.0**** num de protocolo vale: ' . $this->aResposta['protNFe']['infProt']['nProt']);
            $nroProtocolo = $this->aResposta['protNFe']['infProt']['nProt'];
        } else {

            $this->codSefaz  = $this->aResposta['cStat'];
            $this->strMotivo = $this->aResposta['xMotivo'];
            $this->msg       = $this->aResposta['xMotivo'];
            $this->error     = true;

            $this->_setRetornoCliente();
            return $this->provide($this->retornoCliente);
        }

        $xJust = $arrParam['justificativa'];

        try {

            $response = $nfe->sefazCancela($this->chave, $xJust, $nroProtocolo);
            $this->salvarLog('****NFe 4.0**** Retorno cancelamento: ' . print_r($response,true));

            $stdCl = new Standardize($response);
            //nesse caso $std irá conter uma representação em stdClass do XML
            $std = $stdCl->toStd();

            //verifique se o evento foi processado
            if ($std->cStat != 128) {

                $this->error = true;
                $retornoNfe['codSefaz']  = $std->cStat;
                $retornoNfe['strMotivo'] = $std->xMotivo;

            } else {

                $cStat = $std->retEvento->infEvento->cStat;

                if ($cStat == '101' || $cStat == '135') {

                    # SUCESSO PROTOCOLA E GRAVA O XML:
                    $xml = Complements::toAuthorize($nfe->lastRequest, $response);

                    $dir = $this->_getDirNfe() . "/{$this->empresa['pasta_ambiente']}/canceladas/20" . substr($this->chave, 2, 4);
                    $this->_checkDirExists($dir);
                    file_put_contents("$dir/$this->chave-CancNFe-procEvento.xml", $xml);
                    chmod("$dir/$this->chave-CancNFe-procEvento.xml", 0777);

                    $this->error = false;
                } else {
                    $this->error = true;
                }

                $retornoNfe['codSefaz']  = $std->retEvento->infEvento->cStat;
                $retornoNfe['strMotivo'] = $std->retEvento->infEvento->xMotivo;
            }

        } catch(\Exception $e){

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }
        
        $msg = Formata::descMsgSefazPersonalizadas($retornoNfe);

        $this->msg = $this->strMotivo = $msg;
        $this->_setRetornoCliente();

        return $this->provide($this->retornoCliente);
    }

    /**
     * Envia uma carta de correção para SEFAZ
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function criarCce()
    {

        $this->salvarLog('****NFe 4.0**** cliente chamou'.__METHOD__);

        $arrParam = json_decode($this->request->getRawBody(), true);

        if(!empty($this->empresa['tpEmis']) AND $this->empresa['tpEmis'] != FORMA_EMISSAO_NORMAL) {
            return $this->provide(Formulario::getDefaultMessages(ERRO, INSERIR, ['O envio da CCe é possível apenas em forma de emissão normal']));
        }

        if(!isset($arrParam['chave']) OR empty($arrParam['chave'])) {
            return $this->provide(Formulario::getDefaultMessages(ERRO, INSERIR, ['Informe a chave da NF-e que deseja enviar a CCe']));
        }

        if(strlen($arrParam['cce_descricao']) < 15){
            return $this->provide(Formulario::getDefaultMessages(ERRO, INSERIR, ['A descrição deve ter no mínimo 15 caracteres']));
        }

        $this->salvarLog('****NFe 4.0**** Entra criar CCe paramss: ' . print_r($arrParam,true));

        $this->chave = $arrParam['chave'];

        $dir    = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/cartacorrecao/";
        $dirAno = $dir.'20'.substr($this->chave,2,4);

        $this->_checkDirExists($dir);
        $this->_checkDirExists($dirAno);

        $comp = ERRO;

        try {

            $tools = new Tools($this->configuraNFe(), $this->_getCertificado());
            $tools->model('55');

            $response = $tools->sefazCCe($this->chave,$arrParam['cce_descricao'],$arrParam['cce_seq']);

            $stdCl = new Standardize($response);
            $std = $stdCl->toStd();

            # Verifique se o evento foi processado:
            if ($std->cStat != '128') {

                $this->salvarLog('****NFe 4.0**** Houve alguma falha e o evento não foi processado >>> '.print_r($std,true));
                return $this->provide(Formulario::getDefaultMessages(ERRO, ATUALIZAR, 'Erro ao processar o Evento da CCe: '.$std->cStat . '('.$std->retEvento->infEvento->xMotivo.')'));
            } else {

                $this->salvarLog('****NFe 4.0****Evento processado >>> '.print_r($std,true));

                $cStat = $std->retEvento->infEvento->cStat;
                $this->codSefaz  = $cStat;
                $this->strMotivo = $std->retEvento->infEvento->xMotivo;

                if ($cStat == '135' || $cStat == '136') {
                    # SUCESSO PROTOCOLAR A SOLICITAÇÂO ANTES DE GUARDAR:
                    $xml = Complements::toAuthorize($tools->lastRequest, $response);

                    # Grava o XML protocolado:
                    $filename = "$dirAno/{$std->retEvento->infEvento->chNFe}-CCe-procEvento.xml";
                    if(file_put_contents($filename, $xml)) {
                        chmod($filename, 0777);
                    }

                    $comp = SUCESSO;
                }
            }
        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        return $this->provide(Formulario::getDefaultMessages($comp, ATUALIZAR, 'Solicitação de envio de carta de correção enviada para Sefaz e retornada com o seguinte status: '.$this->codSefaz . '('.$this->strMotivo.')'));
    }


    /**
     * Inutilizar uma NFE. Se for A3 manda pro mongo e depois no setRetornoA3 atualiza
     */
    public function inutilizarNfe()
    {

        $this->salvarLog('****NFe 4.0**** chamaaaa a:  ' . __METHOD__);
        $arrParam = json_decode($this->request->getRawBody(), true);
        $aResposta = [];

        $this->salvarLog('****NFe 4.0**** param inutilizar: ' . print_r($arrParam,true));
        //die;
//        $this->_setEmpresa();
        $this->_checkDirExists($this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/inutilizadas/');

        $this->finNroLan = $arrParam['fin_nro_lan'];

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');

        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $this->salvarLog('****NFe 4.0**** vixfin: ' . print_r($this->nota, true));

        $nSerie = 1;
        $nIni = $this->nota['nNF'];
        $nFin = $this->nota['nNF'];
        $xJust = 'Inutilização de notas fiscais';

        $this->salvarLog('****NFe 4.0**** bbbbbb:' . $nSerie . ' - ' . $nIni . ' - ' .$nFin .' - ' . $xJust .' - '  .$this->empresa['tpAmb']);

        $retorno = $nfe->sefazInutiliza($nSerie, $nIni, $nFin, $xJust, $this->empresa['tpAmb']);

        $stdCl = new Standardize($retorno);
        //nesse caso o $arr irá conter uma representação em array do XML retornado
        $aResposta = $stdCl->toArray();

        $this->salvarLog('****NFe 4.0**** retorno $resposta inutilizaçao: ' . print_r($aResposta, true));

        if (isset($aResposta['infInut']['cStat'])) {

            $this->codSefaz = $aResposta['infInut']['cStat'];
            $this->strMotivo = $this->msg = $aResposta['infInut']['xMotivo'];
        }

        if(!isset($arrParam['avulso'])) {

            //verifica se ja foi inutilizada e se faz desfazimento no estoque
            $this->_verificarPosRetornoInutilizar();
        } else {
            $this->_atualizarDataInutilizacao();
        }

        $this->salvarLog('****NFe 4.0**** finCodAcao inutilizar1: ' . $this->finCodAcao);

        $retornoNfe['codSefaz']  = $this->codSefaz;
        $retornoNfe['strMotivo'] = $this->strMotivo;

        $msg = Formata::descMsgSefazPersonalizadas($retornoNfe);

        $this->msg = $this->strMotivo = $msg;

        $this->_setRetornoCliente();

        return $this->provide($this->retornoCliente);
    }

    /**
     * Seta o NCM do produto passado
     * @param $vixPro
     * @return mixed
     */
    protected function _setVixNcm($vixPro)
    {

        $arrFiltro = [
            'filtro' => [
                'ncm_nro_ncm'    => trim($vixPro['pro_ncm']),
                'ncm_uf_destino' => trim($this->destinatario['UFDest']),
                'eh_servico'     => 'N',
                'ncm_tip_consumo_revenda'    => ($this->destinatario['tipContribuinte'] == '1') ? '1' : '0' //0 consumo - 1 revenda
            ]
        ];

        $objVixNcm = VixNcmImp::findFirst($arrFiltro);

        if(!$objVixNcm) {

            $arrFiltro['filtro']['ncm_uf_destino'] = 'Todos';

            $objVixNcm = VixNcmImp::findFirst($arrFiltro);
        }

        return ($objVixNcm) ? $objVixNcm->toArray() : [];
    }

    
    /**
     * Criação de uma nota. É o procedimento inicial de um faturamento 
     */
    protected function _setParametrosCriacao()
    {

        date_default_timezone_set('America/Sao_Paulo');
        
        $arrEnviar['cUF']   = '31';
        $arrEnviar['cNF']   = Formata::preencherStr($this->nota['nNF'],8,0);
        $arrEnviar['nNF']   = $this->nota['nNF'];
        $arrEnviar['natOp'] = Formata::trocarCaracteresEsp(Formata::truncate($this->nota['natOp'],55));
        $arrEnviar['CNPJ']  = Formata::somenteNumeros($this->empresa['CNPJEmitente']);
        $arrEnviar['mod']   = '55';
        $arrEnviar['serie'] = $this->nota['serie'];

        $arrEnviar['tpEmis']       = (empty($this->empresa['tpEmis'])) ? FORMA_EMISSAO_NORMAL : $this->empresa['tpEmis'];
        $arrEnviar['razaoSocial']  = Formata::truncate(Formata::trocarCaracteresEsp($this->empresa['razaoSocialEmitente']),58);
        $arrEnviar['nomeFantasia'] = Formata::truncate(Formata::trocarCaracteresEsp($this->empresa['nomeFantasiaEmitente']),58);
        $arrEnviar['IE']           = ((empty(trim($this->empresa['IEEmitente'])) || preg_match('/isento/',$this->empresa['IEEmitente']))) ? 'ISENTO' : Formata::somenteNumeros($this->empresa['IEEmitente']);
        $arrEnviar['CRT']          = '3';  //1 - simples nacional 2- simples com excesso da receita bruta 3- regime normal
        $arrEnviar['IEST']         = '';
        $arrEnviar['IM']           = '';
        $arrEnviar['CNAE']         = '';
        $arrEnviar['cpf']          = '';

        # endereço do emitente:
        $arrEnviar['xLgr']    = Formata::truncate(Formata::trocarCaracteresEsp(strtoupper($this->empresa['xLgrEmitente'])),60);
        $arrEnviar['nro']     = Formata::trocarCaracteresEsp($this->empresa['nroEmitente']);
        $arrEnviar['xCpl']    = Formata::truncate(Formata::trocarCaracteresEsp($this->empresa['xCplEmitente']),58);
        $arrEnviar['xBairro'] = Formata::truncate(Formata::trocarCaracteresEsp($this->empresa['xBairroEmitente']),58);
        $arrEnviar['cMun']    = $this->empresa['cMunEmitente'];
        $arrEnviar['xMun']    = Formata::truncate(Formata::trocarCaracteresEsp($this->empresa['xMunEmitente']),58);
        $arrEnviar['UF']      = $this->empresa['UFEmitente'];
        $arrEnviar['CEP']     = $this->empresa['CEPEmitente'];
        $arrEnviar['cPais']   = '1058';
        $arrEnviar['xPais']   = 'BRASIL';
        $arrEnviar['fone']    = str_replace([' ','(',')','-'],'',$this->empresa['foneEmitente']);

        # Dados do destinatario:
        $arrEnviar['cnpjDest']      = ($this->destinatario['cliFor'] == 2)?$this->destinatario['cpfcnpjDest']:''; //
        $arrEnviar['cpfDest']       = ($this->destinatario['cliFor'] == 1)?$this->destinatario['cpfcnpjDest']:''; //'10109411609'
        $arrEnviar['xNome']         = Formata::truncate(Formata::trocarCaracteresEsp($this->destinatario['xNomeDest']),58);
        $arrEnviar['idEstrangeiro'] = '';

        # 1-contribuinte icms  2- contribuinte isento  9- nao contribuinte:
        if($this->destinatario['tipContribuinte'] != TIP_CONTRIBUINTE_CONSUMIDOR) {

            if(preg_match('/isento/i',$this->destinatario['IEDest'])) {

                $arrEnviar['indIEDest'] = TIP_CONTRIBUINTE_ISENTO;
            } else {

                $arrEnviar['indIEDest'] = TIP_CONTRIBUINTE_ICMS;
            }

        } else {

            $arrEnviar['indIEDest'] = TIP_CONTRIBUINTE_CONSUMIDOR;
        }

        if($this->destinatario['cliFor'] == 2) {

            $arrEnviar['IEDest'] = Formata::somenteNumeros($this->destinatario['IEDest']);
        } else {
            # Se for pessoa fisica e campo IMunDest tiver preenchido então esse é o codigo do produtor rural que vai pro IEDest:
            if(!empty($this->destinatario['IMunDest'])) {

                $arrEnviar['indIEDest'] = TIP_CONTRIBUINTE_ICMS;
                $arrEnviar['IEDest']    = Formata::somenteNumeros($this->destinatario['IMunDest']);

                # PASSA PARA CONTRIBUINTE PARA NÃO GERAR A TAG ICMS DA UF DE DESTINO:
                $this->destinatario['tipContribuinte'] = TIP_CONTRIBUINTE_ICMS;
            } else {

                $arrEnviar['IEDest'] = '';
            }
        }

        $arrEnviar['ISUFDest']  = '';
        $arrEnviar['IMDest']    = '';
        $arrEnviar['emailDest'] = (!empty($this->destinatario['emailDest'])) ? substr($this->destinatario['emailDest'],0,60) : '';

        # endereço do destinatario:
        $arrEnviar['xLgrDest']    = Formata::truncate(Formata::trocarCaracteresEsp(strtoupper($this->destinatario['xLgrDest'])),58);
        $arrEnviar['nroDest']     = (!empty($this->destinatario['nroDest'])?$this->destinatario['nroDest']:'0');
        $arrEnviar['xCplDest']    = Formata::trocarCaracteresEsp(strtoupper($this->destinatario['xCplDest']));
        $arrEnviar['xBairroDest'] = Formata::trocarCaracteresEsp(strtoupper($this->destinatario['xBairroDest']));

        $arrEnviar['cMunDest']  = (!empty($this->destinatario['cMunDest'])) ? $this->destinatario['cMunDest'] : '';
        $arrEnviar['xMunDest']  = Formata::truncate(Formata::trocarCaracteresEsp($this->destinatario['xMunDest']),58);
        $arrEnviar['UFDest']    = $this->destinatario['UFDest'];
        $arrEnviar['CEPDest']   = $this->destinatario['CEPDest'] = trim($this->destinatario['CEPDest']);
        $arrEnviar['cPaisDest'] = '1058';
        $arrEnviar['xPaisDest'] = 'BRASIL';
        $arrEnviar['foneDest']  = Formata::somenteNumeros($this->destinatario['foneDest']);

        $this->salvarLog('****NFe 4.0**** Captou dados do emitente e do destinatário, $arrEnviar vale: '.print_r($arrEnviar, true));

        $arrEnviar['vICMSTotal']        = 0;
        $arrEnviar['vICMSDesonTotal']   = 0;
        $arrEnviar['vPISTotal']         = 0;
        $arrEnviar['vCOFINSTotal']      = 0;
        $arrEnviar['vIPITotal']         = 0;
        $arrEnviar['vIPIDevolTotal']    = 0;
        $arrEnviar['vIITotal']          = 0;
        $arrEnviar['vFCPTotal']         = 0;
        $arrEnviar['vFCPTotal']         = 0;
        $arrEnviar['vFCPSTTotal']       = 0;
        $arrEnviar['vFCPSTRetTotal']    = 0;
        $arrEnviar['vFCPUFDestTotal']   = 0;
        $arrEnviar['vICMSUFDestTotal']  = 0;
        $arrEnviar['vICMSUFRemetTotal'] = 0;
        
        $this->salvarLog('****NFe 4.0**** uf dest:'.$arrEnviar['UFDest'] . ' e uf orig: ' . $arrEnviar['UF']);
        if($arrEnviar['UFDest'] == $arrEnviar['UF'] AND ($this->nota['cfop'] != '6914') AND ($this->nota['cfop'] != '2914') AND ($this->nota['cfop'] != '6904') AND ($this->nota['cfop'] != '2904')) {
            $this->salvarLog('****NFe 4.0**** vai setar que eh dentro do estado');

            $this->ehForaEstado = false;
            $arrEnviar['idDest'] = '1'; //1=Operação interna; 2=Operação interestadual; 3=Operação com exterior.
        } else {
            $this->salvarLog('****NFe 4.0**** vai setar que eh fora do estado');
            $this->ehForaEstado = true;
            $arrEnviar['idDest'] = '2';

            # Caso seja fora do estado e seja exportação, ajusta os parametros
            if($this->notaExterior) {
                $this->ehForaEstado = false;
                $arrEnviar['idDest'] = '3';
            }
        }

        $arrEnviar['vTotalBCICMS'] = $arrEnviar['vTotalBCPIS'] = 0;
        $arrEnviar['vBCST'] = 0;
        $arrEnviar['vST'] = 0;

        //1=NF-e normal; 2=NF-e complementar; 3=NF-e de ajuste; 4=Devolução/Retorno.
        if(isset($this->nota['finNFe']) AND !empty($this->nota['finNFe'])) {
            $arrEnviar['finNFe'] = $this->nota['finNFe'];
        }

        //produtos
        foreach($this->nota['itens'] as $i => $itensProduto)
        {
            
            $arrEnviar['aP'][$i] = [
                'nItem'             => $itensProduto['nItem'],
                'cProd'             => $itensProduto['cProd'],
                'cEAN'              => 'SEM GTIN',
                'xProd'             => Formata::truncate(Formata::trocarCaracteresEsp($itensProduto['xProd']),115),
                'NCM'               => $itensProduto['NCM'],
                'NVE'               => '',
                'CEST'              => (!empty($itensProduto['CEST']))            ? $itensProduto['CEST'] : '',
                'CNPJFab'           => (!empty($itensProduto['CNPJFab'])) ? $itensProduto['CNPJFab'] : '',
                'indEscala'         => (!empty($itensProduto['CNPJFab'])) ? 'N' : 'S', # S - Produzido em Escala Relevante; N – Produzido em Escala NÃO Relevante.
                'cBenef'            => '',
                'EXTIPI'            => (!empty($itensProduto['EXTIPI']))         ? trim($itensProduto['EXTIPI']) : '',
                'CFOP'              => $itensProduto['CFOP'],
                'uCom'              => $itensProduto['uCom'],
                'qCom'              => Formata::toFloatPontoNota($itensProduto['qCom'],'',4),
                'vUnCom'            => Formata::toFloatPontoNota($itensProduto['vUnCom'],'',10),
                'vProd'             => Formata::toFloatPontoNota($itensProduto['vProd'],'',2),
                'cEANTrib'          => 'SEM GTIN',
                'uTrib'             => $itensProduto['uCom'],
                'qTrib'             => Formata::toFloatPontoNota($itensProduto['qCom'],'',4),
                'vUnTrib'           => Formata::toFloatPontoNota($itensProduto['vProd']/$itensProduto['qCom'],'',4),
                'vFrete'            => Formata::toFloatPontoNota($itensProduto['vFrete']),
                'vSeg'              => Formata::toFloatPontoNota($itensProduto['vSeg']),
                'vDesc'             => ($itensProduto['vDesc'] > 0) ? Formata::toFloatPontoNota($itensProduto['vDesc']) : null,
                'vOutro'            => ($itensProduto['vOutro'] > 0) ? Formata::toFloatPontoNota($itensProduto['vOutro']) : null,
                'infAdProd'         => Formata::truncate(Formata::trocarCaracteresEsp($itensProduto['infAdProd']),495),
                'indTot'            => '1', //se vai somar ou nao no valor total da nota... quando for nota complementar ou de ajuste informar zero. Verificar o campo finNFe.. como ta 1 é nota normal
                'orig'              => $itensProduto['orig'],
                'modBC'             => $itensProduto['modBC'],
                'vBC'               => '0.00',
                'pRedBC'            => '0.00',
                'pICMS'             => '0.00',
                'vICMSDeson'        => (isset($itensProduto['vICMSDeson'])) ? $itensProduto['vICMSDeson'] : '0.00',
                'motDesICMS'        => (isset($itensProduto['motDesICMS'])) ? $itensProduto['motDesICMS'] : '9',
                'vICMS'             => '0.00',
                'pMVAST'            => '',
                'pDif'              => (isset($itensProduto['pDif'])) ? $itensProduto['pDif'] : '0.00',
                'vICMSDif'          => (isset($itensProduto['vICMSDif'])) ? $itensProduto['vICMSDif'] : '0.00',
                'vICMSOp'           => (isset($itensProduto['vICMSOp'])) ? $itensProduto['vICMSOp'] : '0.00',
                'vBCSTRet'          => (isset($itensProduto['vBCSTRet'])) ? $itensProduto['vBCSTRet'] : '0.00',
                'pST'               => (isset($itensProduto['pST'])) ? $itensProduto['pST'] : '0.00',
                'vICMSSTRet'        => (isset($itensProduto['vICMSSTRet'])) ? $itensProduto['vICMSSTRet'] : '0.00',
                'vICMSSTDest'       => '0.00',
                'vBCSTDest'         => '0.00',
                'pCredSN'           => '0.00',
                'vCredICMSSN'       => '0.00',
                'cstPIS'            => '49',
                'cstCOFINS'         => '49',
                'vBCPIS'            => '0.00',
                'vBCCOFINS'         => '0.00',
                'pPIS'              => '0.00',
                'vPIS'              => '0.00',
                'vAliqProd'         => null,
                'qBCProd'           => null,
                'pCOFINS'           => '0.00',
                'vCOFINS'           => '0.00',
                'xPed'              => '',
                'nItemPed'          => ''
            ];

            if(isset($itensProduto['vICMSDeson'])) {
                $arrEnviar['vICMSDesonTotal'] = $arrEnviar['vICMSDesonTotal'] + (float) $itensProduto['vICMSDeson'];
            }

            # Verifica se o campo código do benefício atende ao padrão:
            if(!empty($itensProduto['cBenef'])) {
                if(strlen(trim($itensProduto['cBenef'])) == 10) {
                    $arrEnviar['aP'][$i]['cBenef'] = (string) trim($itensProduto['cBenef']);
                }
            }

            # Verifica qual é o padrão do código de barras informado:
            if(in_array(strlen(trim($itensProduto['cEAN'])), [8,12,13,14])) {
                $arrEnviar['aP'][$i]['cEANTrib'] = $arrEnviar['aP'][$i]['cEAN'] = trim($itensProduto['cEAN']);
            }

            if($arrEnviar['finNFe'] == FINALIDADE_COMPLEMENTAR) {

                $arrEnviar['aP'][$i]['cProd'] = 'CFOP'.$itensProduto['CFOP'];
                $arrEnviar['aP'][$i]['NCM']   = '00000000';
            }

            if($itensProduto['xPed'] >= 0) {
                $arrEnviar['aP'][$i]['xPed'] = $itensProduto['xPed'];
            }

            if($itensProduto['nItemPed'] >= 0) {
                $arrEnviar['aP'][$i]['nItemPed'] = $itensProduto['nItemPed'];
            }

            $arrEnviar['aP'][$i]['vTotTrib'] = 0;
            if($this->destinatario['tipContribuinte'] == TIP_CONTRIBUINTE_CONSUMIDOR AND $this->nota['codNatOp'] == NATUREZA_VENDA) {

                $aliqIbpt = 0.3209;
                $arrEnviar['aP'][$i]['vTotTrib'] = Formata::toFloatPontoNota($itensProduto['vProd'] * $aliqIbpt);
                $this->salvarLog('****NFe 4.0**** eh contribuinte entao vai setar o valor liquido no IBPT? vtottrib: '.$arrEnviar['aP'][$i]['vTotTrib'] );
            }


            $arrEnviar['aP'][$i]['vBCUFDest']    = '0.00';
            $arrEnviar['aP'][$i]['pFCPUFDest']   = null;
            $arrEnviar['aP'][$i]['pICMSUFDest']  = (isset($itensProduto['pICMSUFDest'])) ? $itensProduto['pICMSUFDest'] : null;
            $arrEnviar['aP'][$i]['vFCPUFDest']   = null;
            $arrEnviar['aP'][$i]['vICMSUFDest']  = null;
            $arrEnviar['aP'][$i]['vICMSUFRemet'] = null;
            $arrEnviar['aP'][$i]['vBCFCPUFDest'] = null;

            $arrOr = [1, 2, 3, 6];

            if(in_array($itensProduto['orig'],$arrOr)) {
                $arrEnviar['aP'][$i]['pICMSInter'] = $itensProduto['pICMSInter'];

            } else {
                $arrTabAliqInter = Parametros::tabelaAliquotaInterestadual();
                $arrEnviar['aP'][$i]['pICMSInter'] = Formata::toFloatPontoNota($arrTabAliqInter[$arrEnviar['UF']][$arrEnviar['UFDest']]);
            }

            $ano = date('Y');

            # Caso seja uma nota de devolução, extrai o ano da chave de referencia para pegar a aliquota correta,
            # pois a receita está valodando com base na chave de origem:
            if($this->nota['finNFe'] == FINALIDADE_DEVOLUCAO AND (isset($this->nota['refNFe']) AND !empty($this->nota['refNFe']))) {
                $ano = '20' . substr($this->nota['refNFe'], 2, 2);
            }

            $arrEnviar['aP'][$i]['pICMSInterPart'] = Parametros::getPorcPartilha($ano);

            # Seta valores padrões para os impostos abaixo:
            $arrEnviar['aP'][$i]['vBCFCPST']    = null;
            $arrEnviar['aP'][$i]['pFCPST']      = null;
            $arrEnviar['aP'][$i]['vFCPST']      = null;
            $arrEnviar['aP'][$i]['vBCFCPSTRet'] = null;
            $arrEnviar['aP'][$i]['pFCPSTRet']   = null;
            $arrEnviar['aP'][$i]['vFCPSTRet']   = null;
            $arrEnviar['aP'][$i]['pFCP']        = null;
            $arrEnviar['aP'][$i]['vFCP']        = null;
            $arrEnviar['aP'][$i]['vBCFCP']      = null;

            $arrEnviar['aP'][$i]['CST'] = $itensProduto['CST'];  //$itensProduto['ite_cst'], //'01', //pro_cst2
            $arrEnviar['aP'][$i]['vBC'] = Formata::toFloatPontoNota($itensProduto['vBC'],'',2);

            $arrEnviar['aP'][$i]['pRedBC'] = abs($itensProduto['pRedBC']);

            if($arrEnviar['aP'][$i]['pRedBC'] == 0) {
                $arrEnviar['aP'][$i]['pRedBC'] = '0.00';
            }

            $arrEnviar['aP'][$i]['pICMS'] = Formata::toFloatPontoNota($itensProduto['pICMS'],'',2);
            $arrEnviar['aP'][$i]['vICMS'] = Formata::toFloatPontoNota($itensProduto['vICMS'],'',2);

            if($this->ehForaEstado) {
                if($this->destinatario['tipContribuinte'] == TIP_CONTRIBUINTE_CONSUMIDOR) {
                    $arrEnviar['aP'][$i]['vBCUFDest']    = Formata::toFloatPontoNota($itensProduto['vBCUFDest'],2);
                    $vlrICMSInter                        = Formata::toFloatPontoNota($arrEnviar['aP'][$i]['vBCUFDest'] * ((float) $arrEnviar['aP'][$i]['pICMSInter'] / 100));
                    $arrEnviar['aP'][$i]['vICMSUFDest']  = Formata::toFloatPontoNota($vlrICMSInter * ((float) $arrEnviar['aP'][$i]['pICMSInterPart'] / 100),2);
                    $arrEnviar['aP'][$i]['vICMSUFRemet'] = Formata::toFloatPontoNota($vlrICMSInter - $arrEnviar['aP'][$i]['vICMSUFDest'],2);

                    $arrEnviar['vICMSUFDestTotal']  = $arrEnviar['vICMSUFDestTotal'] + (float) $arrEnviar['aP'][$i]['vICMSUFDest'];
                    $arrEnviar['vICMSUFRemetTotal'] = $arrEnviar['vICMSUFRemetTotal'] + (float) $arrEnviar['aP'][$i]['vICMSUFRemet'];
                }
            }

            # Acumula no totalizador do ICMS:
            $arrEnviar['vICMSTotal']   = ($arrEnviar['vICMSTotal'] + $arrEnviar['aP'][$i]['vICMS']);
            $arrEnviar['vTotalBCICMS'] = $arrEnviar['vTotalBCICMS'] + $itensProduto['vBC'];

            $arrEnviar['aP'][$i]['CST']  = Formata::preencherStr($itensProduto['CST'], 2, 0);
            $arrEnviar['aP'][$i]['vBCST']    = (isset($itensProduto['vBCST'])) ? Formata::toFloatPontoNota($itensProduto['vBCST'],'',2) : '0.00';
            $arrEnviar['aP'][$i]['modBCST']  = (isset($itensProduto['modBCST'])) ? $itensProduto['modBCST'] : '0.00';
            $arrEnviar['aP'][$i]['pMVAST']   = (isset($itensProduto['pMVAST'])) ? $itensProduto['pMVAST'] : '0.00';
            $arrEnviar['aP'][$i]['pRedBCST'] = (isset($itensProduto['pRedBCST'])) ? $itensProduto['pRedBCST'] : '0.00';
            $arrEnviar['aP'][$i]['pICMSST']  = (isset($itensProduto['pICMSST'])) ? Formata::toFloatPontoNota($itensProduto['pICMSST'],'',2) : '0.00';
            $arrEnviar['aP'][$i]['vICMSST']  = (isset($itensProduto['vICMSST'])) ? Formata::toFloatPontoNota($itensProduto['vICMSST'],'',2) : '0.00';

            # Acumula no totalizador da ST:
            $arrEnviar['vST'] = $arrEnviar['vST'] + $arrEnviar['aP'][$i]['vICMSST'];
            $arrEnviar['vBCST'] = $arrEnviar['vBCST'] + $arrEnviar['aP'][$i]['vBCST'];

            if(isset($itensProduto['cstIPI']) AND !empty($itensProduto['cstIPI'])) {

                $arrEnviar['aP'][$i]['cstIPI'] = Formata::preencherStr($itensProduto['cstIPI'], 2, 0);

                if($this->nota['finNFe'] == FINALIDADE_DEVOLUCAO) {

                    $arrEnviar['aP'][$i]['pDevol']    = $itensProduto['pDevol'];
                    $arrEnviar['aP'][$i]['vIPIDevol'] = $itensProduto['vIPIDevol'];

                    $arrEnviar['vIPIDevolTotal'] = $arrEnviar['vIPIDevolTotal'] + $arrEnviar['aP'][$i]['vIPIDevol'];

                } else {

                    $arrEnviar['aP'][$i]['vBCIPI'] = Formata::toFloatPontoNota($itensProduto['vBCIPI'],'',2);
                    $arrEnviar['aP'][$i]['pIPI']   = Formata::toFloatPontoNota($itensProduto['pIPI'],'',2);
                    $arrEnviar['aP'][$i]['vIPI']   = Formata::toFloatPontoNota($itensProduto['vIPI'],'',2);
                    $arrEnviar['aP'][$i]['cEnq']   = $itensProduto['cEnq'];

                    # Acumula no totalizador do IPI:
                    $arrEnviar['vIPITotal'] = $arrEnviar['vIPITotal'] + $arrEnviar['aP'][$i]['vIPI'];
                }
            }

            if(isset($itensProduto['cstPIS']) AND !empty($itensProduto['cstPIS'])) {

                $arrEnviar['aP'][$i]['cstPIS'] = Formata::preencherStr($itensProduto['cstPIS'], 2, 0);
                $arrEnviar['aP'][$i]['vPIS']   = Formata::toFloatPontoNota($itensProduto['vPIS'],'',2);
                $arrEnviar['aP'][$i]['vBCPIS'] = Formata::toFloatPontoNota($itensProduto['vBCPIS'],'',2);
                $arrEnviar['aP'][$i]['pPIS']   = Formata::toFloatPontoNota($itensProduto['pPIS'],'',2);

                $arrEnviar['vPISTotal']   = $arrEnviar['vPISTotal'] + $arrEnviar['aP'][$i]['vPIS'];
            }

            $arrEnviar['vTotalBCPIS'] = $arrEnviar['vTotalBCPIS'] + $itensProduto['vBCPIS'];

            if(Formata::preencherStr($itensProduto['CST'], 2, 0) == '03') {
                $arrEnviar['aP'][$i]['qBCProd']   = Formata::toFloatPontoNota($itensProduto['qBCProd'],'',4);
                $arrEnviar['aP'][$i]['vAliqProd'] = $itensProduto['vAliqProd'];
            } else {
                $arrEnviar['aP'][$i]['qBCProd']   = null;
                $arrEnviar['aP'][$i]['vAliqProd'] = null;
            }

            if(isset($itensProduto['cstCOFINS']) AND !empty($itensProduto['cstCOFINS'])) {

                $arrEnviar['aP'][$i]['cstCOFINS'] = Formata::preencherStr($itensProduto['cstCOFINS'], 2, 0);
                $arrEnviar['aP'][$i]['vBCCOFINS'] = Formata::toFloatPontoNota($itensProduto['vBCCOFINS'],'',2);
                $arrEnviar['aP'][$i]['pCOFINS']   = Formata::toFloatPontoNota($itensProduto['pCOFINS'],'',2);
                $arrEnviar['aP'][$i]['vCOFINS']   = Formata::toFloatPontoNota($itensProduto['vCOFINS'],'',2);

                $arrCst1 = ['04','05','06','07','08','09'];

                if(!in_array($itensProduto['cstCOFINS'],$arrCst1)) {
                    $arrEnviar['vCOFINSTotal'] = (float) $arrEnviar['vCOFINSTotal'] + (float) $arrEnviar['aP'][$i]['vCOFINS'];
                }
            }

            # Se for retido, quer dizer que vai somar nos totais da nota, se não for, quer dizer que é meramente informativo:
            if(isset($itensProduto['vFCPST']) AND !empty($itensProduto['vFCPST'])) {
                $arrEnviar['aP'][$i]['vBCFCPST'] = Formata::toFloatPontoNota($itensProduto['vBCFCPST']);
                $arrEnviar['aP'][$i]['pFCPST']   = Formata::toFloatPontoNota($itensProduto['pFCPST']);
                $arrEnviar['aP'][$i]['vFCPST']   = Formata::toFloatPontoNota($itensProduto['vFCPST']);

                $arrEnviar['vFCPSTTotal'] = (float) $arrEnviar['vFCPSTTotal'] + (float) $arrEnviar['aP'][$i]['vFCPST'];
            }

            if(isset($itensProduto['vFCPSTRet']) AND !empty($itensProduto['vFCPSTRet'])) {
                $arrEnviar['aP'][$i]['vBCFCPSTRet'] = Formata::toFloatPontoNota($itensProduto['vBCFCPSTRet']);
                $arrEnviar['aP'][$i]['pFCPSTRet']   = Formata::toFloatPontoNota($itensProduto['pFCPSTRet']);
                $arrEnviar['aP'][$i]['vFCPSTRet']   = Formata::toFloatPontoNota($itensProduto['vFCPSTRet']);

                $arrEnviar['vFCPSTRetTotal'] = (float) $arrEnviar['vFCPSTRetTotal'] + (float) $arrEnviar['aP'][$i]['vFCPSTRet'];
            }


            if($this->ehForaEstado) {

                $arrEnviar['aP'][$i]['vBCFCPUFDest'] = Formata::toFloatPontoNota($itensProduto['vBCFCPUFDest']);
                $arrEnviar['aP'][$i]['pFCPUFDest']   = Formata::toFloatPontoNota($itensProduto['pFCPUFDest']);
                $arrEnviar['aP'][$i]['vFCPUFDest']   = Formata::toFloatPontoNota($itensProduto['vFCPUFDest']);

                $arrEnviar['vFCPUFDestTotal'] = (float) $arrEnviar['vFCPUFDestTotal'] + (float) $arrEnviar['aP'][$i]['vFCPUFDest'];
            } else {

                $arrEnviar['aP'][$i]['pFCP']   = (isset($itensProduto['pFCP'])) ? Formata::toFloatPontoNota($itensProduto['pFCP']) : null;
                $arrEnviar['aP'][$i]['vFCP']   = (isset($itensProduto['vFCP'])) ? Formata::toFloatPontoNota($itensProduto['vFCP']) : null;
                $arrEnviar['aP'][$i]['vBCFCP'] = (isset($itensProduto['vBCFCP'])) ? Formata::toFloatPontoNota($itensProduto['vBCFCP']) : null;

                $arrEnviar['vFCPTotal'] = (float) $arrEnviar['vFCPTotal'] + (float) $arrEnviar['aP'][$i]['vFCP'];
            }


            $this->salvarLog('****NFe 4.0****  $arrEnviar após foreach dos produtos>>>> '.print_r($arrEnviar, true));
        }

        $arrEnviar['vICMSDeson'] = '0.00';
        $arrEnviar['vProd']      = $this->nota['vProd']; //'20.00';
        $arrEnviar['vFrete']     = $this->nota['vFrete'];
        $arrEnviar['vSeg']       = $this->nota['vSeg'];
        $arrEnviar['vDesc']      = $this->nota['vDesc'];
        $arrEnviar['vII']        = $arrEnviar['vIITotal'];
        $arrEnviar['vIPI']       = '0.00';
        $arrEnviar['vPIS']       = '0.00';
        $arrEnviar['vCOFINS']    = '0.00';
        $arrEnviar['vOutro']     = $this->nota['vOutro'];
        $arrEnviar['vNF']        = $this->nota['vNF']; //'20.00';
        $arrEnviar['vTotTrib']   = 0; //32.09;
        $arrEnviar['modFrete']   = $this->nota['modFrete']; //0=Por conta do emitente; 1=Por conta do destinatário/remetente; 2=Por conta de terceiros;
        $arrEnviar['infAdFisco'] = '';
        $arrEnviar['infCpl']     = $this->nota['infCpl']; //informaçao complementar da nota
        $arrEnviar['dhCont']     = '';
        $arrEnviar['xJust']      = ''; //Justificativa da entrada em contingência

        # Totais da nota:
        $arrEnviar['vICMSTotal']        = Formata::toFloatPontoNota($arrEnviar['vICMSTotal'],'',2);
        $arrEnviar['vICMSDesonTotal']   = Formata::toFloatPontoNota($arrEnviar['vICMSDesonTotal'],'',2);
        $arrEnviar['vIPITotal']         = Formata::toFloatPontoNota($arrEnviar['vIPITotal'],'',2);
        $arrEnviar['vIPIDevolTotal']    = Formata::toFloatPontoNota($arrEnviar['vIPIDevolTotal'],'',2);
        $arrEnviar['vCOFINSTotal']      = Formata::toFloatPontoNota($arrEnviar['vCOFINSTotal'],'',2);
        $arrEnviar['vPISTotal']         = Formata::toFloatPontoNota($arrEnviar['vPISTotal'],'',2);
        $arrEnviar['vST']               = Formata::toFloatPontoNota($arrEnviar['vST'],'',2);
        $arrEnviar['vBCST']             = Formata::toFloatPontoNota($arrEnviar['vBCST'],'',2);
        $arrEnviar['vTotalBCICMS']      = Formata::toFloatPontoNota($arrEnviar['vTotalBCICMS'],'',2);
        $arrEnviar['vIITotal']          = Formata::toFloatPontoNota($arrEnviar['vIITotal'], '', 2);
        $arrEnviar['vFCPSTRetTotal']    = Formata::toFloatPontoNota($arrEnviar['vFCPSTRetTotal'], '', 2);
        $arrEnviar['vFCPSTTotal']       = Formata::toFloatPontoNota($arrEnviar['vFCPSTTotal'], '', 2);
        $arrEnviar['vFCPTotal']         = Formata::toFloatPontoNota($arrEnviar['vFCPTotal'], '', 2);
        $arrEnviar['vFCPUFDestTotal']   = Formata::toFloatPontoNota($arrEnviar['vFCPUFDestTotal'], '', 2);
        $arrEnviar['vICMSUFDestTotal']  = Formata::toFloatPontoNota($arrEnviar['vICMSUFDestTotal']);
        $arrEnviar['vICMSUFRemetTotal'] = Formata::toFloatPontoNota($arrEnviar['vICMSUFRemetTotal']);

        # Verifica se existe registro de contingencia:
        $fileContingencia = $this->_getDirNfe() . "/extras/contingencia.json";
        $jsonContingencia = '';
        if(file_exists($fileContingencia)) {
            $jsonContingencia = file_get_contents($fileContingencia);
        }

        # Caso tenha registro de contingência e a forma de emissão estiver normal,
        # limpa o registro de contingencia e desativa na biblioteca:
        if(!empty($jsonContingencia) AND $arrEnviar['tpEmis'] == FORMA_EMISSAO_NORMAL) {
            
            $contingency = new Contingency($jsonContingencia);
            $contingency->deactivate();
            
            unlink($fileContingencia);
        }

        # Verifica se existe contingencia ativada:
        if($arrEnviar['tpEmis'] == FORMA_EMISSAO_CONTINGENCIA_DPEC || $arrEnviar['tpEmis'] == FORMA_EMISSAO_CONTINGENCIA_SVC_AN || $arrEnviar['tpEmis'] == FORMA_EMISSAO_CONTINGENCIA_SVC_RS) {
            $this->salvarLog('****NFe 4.0**** Entra para ativar o modo de contingência: '. $arrEnviar['tpEmis']);

            if(!empty($jsonContingencia)) {
                new Contingency($jsonContingencia);

                $objCont = json_decode($jsonContingencia);

                $dtCont = date('Y-m-d H:i:sP', $objCont->timestamp);
                $arrEnviar['dhCont'] = str_replace(' ', 'T', $dtCont);
                $arrEnviar['xJust']  = $objCont->motive;
            } else {

                $arrEnviar['dhCont'] = str_replace(' ', 'T', date('Y-m-d H:i:sP', strtotime("-2 seconds"))); //entrada em contingência AAAA-MM-DDThh:mm:ssTZD
                $arrEnviar['xJust']  = 'Servidores da SEFAZ indisponíveis sem previsão de retorno.'; //Justificativa da entrada em contingência

                $contingency = new Contingency();
                $status = $contingency->activate($this->empresa['UFEmitente'], $arrEnviar['xJust'], Formata::getDescricaoTipoContingencia($arrEnviar['tpEmis']));

                if(file_put_contents($fileContingencia, $status)) {
                    chmod($fileContingencia, 0777);
                }
            }
        }

        # Formata a data de emissão passada no parametro ou cria a data de emissão da nota:
        if(!empty($this->nota['dhEmi'])) {

            $d = $this->nota['dhEmi'].date("P");
            $arrEnviar['dhEmi'] = str_replace(" ", "T", $d);

        } else {
            $arrEnviar['dhEmi'] = str_replace(" ", "T", date("Y-m-d H:i:sP"));
        }

        $tempData = explode("-", $arrEnviar['dhEmi']);
        $arrEnviar['ano'] = $tempData[0] - 2000;
        $arrEnviar['mes'] = $tempData[1];
        # $arrEnviar['indPag'] = '0'; //0=Pagamento à vista; 1=Pagamento a prazo; 2=Outros REMOVIDO

        if(!empty($this->nota['dhSaiEnt'])) {

            $d = $this->nota['dhSaiEnt'].date("P");
            $arrEnviar['dhSaiEnt'] = str_replace(" ", "T", $d);

        } else {
            $arrEnviar['dhSaiEnt'] = ''; //str_replace(" ", "T", date("Y-m-d H:i:sP"));
        }

//        if($this->nota['codNatOp'] == NATUREZA_VENDA || $this->nota['codNatOp'] == NATUREZA_DEVOLUCAO_COMPRA || $this->nota['codNatOp'] == NATUREZA_OUTRAS_SAIDAS) {
            $arrEnviar['tpNF'] = $this->nota['tpNF'];
//        } else {
//            $arrEnviar['tpNF'] = '0';
//        }

        $arrEnviar['cMunFG'] = $this->empresa['cMunEmitente'];
        $arrEnviar['tpImp']  = '1'; //0=Sem geração de DANFE; 1=DANFE normal, Retrato; 2=DANFE normal, Paisagem;
        $arrEnviar['cDV']    = '4'; //digito verificador
        $arrEnviar['tpAmb']  = $this->empresa['tpAmb']; //1=Produção; 2=Homologação

        //1-contribuinte icms  2- contribuinte isento  9- nao contribuinte)
        if($this->destinatario['tipContribuinte'] == TIP_CONTRIBUINTE_CONSUMIDOR) {
            $arrEnviar['indFinal'] = '1'; //0=Não; 1=Consumidor final;
        } else {
            $arrEnviar['indFinal'] = '0';
        }

        $arrEnviar['indPres'] = '1';
        if(substr($this->nota['cfop'],0,2) == '14') {
            $arrEnviar['indPres'] = '9';
        }

        $arrEnviar['procEmi'] = '0'; //0=Emissão de NF-e com aplicativo do contribuinte;
        $arrEnviar['verProc'] = '3.22.8'; //versão do aplicativo emissor
        $arrEnviar['versao']  = '4.00';

        if($arrEnviar['modFrete'] != 9) {

            $finTransportadora = $this->nota['transportadora'];
            $transporte        = $this->nota['transporte'];

            $this->salvarLog('****NFe 4.0**** $finTransportadora: ' . print_r($finTransportadora,true));
            $this->salvarLog('****NFe 4.0**** $transporte: ' . print_r($transporte,true));

            if(isset($finTransportadora['cpfcnpjDest'])) {

                $arrEnviar['numCNPJTransp'] = ($finTransportadora['pfPj'] == TIP_PESSOA_JURIDICA) ? Formata::somenteNumeros($finTransportadora['cpfcnpjTransp']) : '';
                $arrEnviar['numCPFTransp']  = ($finTransportadora['pfPj'] == TIP_PESSOA_FISICA) ? Formata::somenteNumeros($finTransportadora['cpfcnpjTransp']) : '';
                $arrEnviar['xNomeTransp']   = Formata::truncate(Formata::trocarCaracteresEsp($finTransportadora['xNomeTransp']),58);
                $arrEnviar['numIETransp']   = Formata::somenteNumeros($finTransportadora['numIETransp']);
                $arrEnviar['xEnderTransp']  = Formata::truncate(Formata::trocarCaracteresEsp($finTransportadora['xEnderTransp'] .' ' . $finTransportadora['nroTransp'].' '.$finTransportadora['xCplTransp'].' - '.$finTransportadora['xBairroTransp']),55);
                $arrEnviar['xMunTransp']    = Formata::trocarCaracteresEsp($finTransportadora['xMunTransp']);
                $arrEnviar['siglaUF']       = $finTransportadora['siglaUF'];
                $arrEnviar['placaVeic']     = $transporte['placaVeic'];
                $arrEnviar['rntc']          = '';
                $arrEnviar['siglaUFVeic']   = $transporte['siglaUFVeic'];

            } else {

                $arrEnviar['xNomeTransp'] = '';
                if($arrEnviar['modFrete'] == 1 OR $arrEnviar['modFrete'] == 2) {
                    $arrEnviar['xNomeTransp'] = 'DESTINATARIO/REMETENTE';
                }
            }

            # Informa a os dados dos volumes transportados:
            $arrEnviar['qVolTransp']  = (!empty($transporte['qVolTransp']))        ? $transporte['qVolTransp'] : '0';
            $arrEnviar['espTransp']   = (!empty($transporte['espTransp']))    ? $transporte['espTransp'] : '0';
            $arrEnviar['nVolTransp']  = (!empty($transporte['nVolTransp']))        ? $transporte['nVolTransp'] : '0';
            $arrEnviar['pesoLTransp'] = (!empty($transporte['pesoLTransp']))   ? $transporte['pesoLTransp'] : '0';
            $arrEnviar['pesoBTransp'] = (!empty($transporte['pesoBTransp'])) ? $transporte['pesoBTransp'] : '0';
            $arrEnviar['marcaTransp'] = '';
        }

        $this->salvarLog('****NFe 4.0**** tem parcela.. comecando a montar o array');
        $arrEnviar['nFat']      = $arrEnviar['nNF'];
        $arrEnviar['vOrig']     = $arrEnviar['vNF'];
        $arrEnviar['vDescFat']  = '0.00';
        $arrEnviar['vLiq']      = $arrEnviar['vNF'];
        $arrEnviar['tPag']      = Formata::getTipoPgtoNFe(isset($this->nota['tPag']) ? $this->nota['tPag'] : '');
        $arrEnviar['vPag']      = $arrEnviar['vNF'];
        $arrEnviar['tBand']     = '';
        $arrEnviar['cAut']      = '';
        $arrEnviar['tpIntegra'] = ($arrEnviar['tPag'] == '03' OR $arrEnviar['tPag'] == '04') ? '2' : ''; # 2= Pagamento não integrado com o sistema de automação da empresa (Ex.: equipamento POS);
        $arrEnviar['CNPJCredenciadora'] = '';

        if($arrEnviar['finNFe'] == FINALIDADE_AJUSTE OR $arrEnviar['finNFe'] == FINALIDADE_DEVOLUCAO) {
            
            $arrEnviar['nFat']      = '';
            $arrEnviar['vDescFat']  = '0.00';
            $arrEnviar['tpIntegra'] = '';
            $arrEnviar['vPag']      = '0';
            $arrEnviar['tPag']      = '90';
        }
        
        if($arrEnviar['finNFe'] == FINALIDADE_DEVOLUCAO) {
            $arrEnviar['refNFe'] = $this->nota['refNFe'];
        }

        # Maranhão não aceita transação sem integração
        if(trim($this->destinatario['UFDest']) == 'MA') {
            $arrEnviar['tpIntegra'] = '';
        }

        if(isset($this->nota['parcelas']) AND count($this->nota['parcelas'])) {

            foreach($this->nota['parcelas'] as $k => $parcela)
            {
                $this->salvarLog('****NFe 4.0**** vai fazer  parcela');
                $arrEnviar['parcela'][$k]['nDup']  = Formata::preencherStr($parcela['nDup'],3,0);
                $arrEnviar['parcela'][$k]['dVenc'] = explode(' ',$parcela['dVenc'])[0];
                $arrEnviar['parcela'][$k]['vDup']  = $parcela['vDup'];
            }
        }

        $this->salvarLog('****NFe 4.0**** terminou de setar os parametros, $arrEnviar vale: ' .print_r($arrEnviar,true));

//        print_r($arrEnviar);die;
        $this->arrEnviar = $arrEnviar;
    }

    /**
     * Retorna um especifico xml de entrada
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function getXmlAprovado($folder = 'aprovadas',$chave)
    {

        $this->chave = $chave;
        $this->empresa['tpAmb'] = NFE_AMBIENTE_HOMOLOGACAO;

        $arqXmlAutorizado = $this->_getXmlAprovado();

        if(!isset($this->searchFields['tipo_retorno']) || $this->searchFields['tipo_retorno'] == 1) {
            
            header('Content-type: "text/xml"; charset="utf8"');
            header('Content-Disposition: attachment;filename='.$this->chave .'.xml');
            header('Content-Length: '.filesize($arqXmlAutorizado));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            ob_clean();
            flush();
            readfile($arqXmlAutorizado);
            exit;

        } else {

            $xml = file_get_contents($arqXmlAutorizado);
            $retorno['xml'] = $xml;
            return $this->provide($retorno);
        }
    }

    /**
     * Retorna um especifico xml cancelado
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function getXmlCancelado()
    {

        $this->chave = $this->searchFields['chave'];

        $this->_setEmpresa(); //die;

        $arqXmlCancelado = $this->_getXmlCancelado();

        if(!isset($this->searchFields['tipo_retorno']) || $this->searchFields['tipo_retorno'] == 1) {

            header('Content-type: "text/xml"; charset="utf8"');
            header('Content-Disposition: attachment;filename='.$this->chave .'.xml');
            header('Content-Length: '.filesize($arqXmlCancelado));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            ob_clean();
            flush();
            readfile($arqXmlCancelado);
            exit;

        } else {

            $xml = file_get_contents($arqXmlCancelado);
            $retorno['xml'] = $xml;
            return $this->provide($retorno);
        }
    }

    /**
     * Retorna um especifico xml de entrada
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function getXmlEntrada()
    {
        
        $chave = $this->searchFields['chave'];

        $this->salvarLog('****NFe 4.0**** TESTE DE XML DE ENTRADA: '. print_r($this->decodedJwt,true));

        $this->_setEmpresa(); //die;

        $filePath = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/entradas/'.$chave.'-nfe.xml';

        $handle       = fopen($filePath, 'r');
        $fileContents = fread($handle, filesize($filePath));

        $fileContents = Formata::excluirAcentos($fileContents);

        $retorno[0] = $fileContents;

       // $this->salvarLog('****NFe 4.0**** xml entrada:'.print_r($retorno,true));

        return $this->provide($retorno);

    }

    public function getXmlEntradaDownload()
    {

        $this->chave = $this->searchFields['chave'];
        $token = $this->searchFields['ken'];

        $this->salvarLog('****NFe 4.0**** TESTE DE XML DE ENTRADA download: '. print_r($this->decodedJwt,true));

        //TODO implementar segurança pra somente a empresa dele conseguir imprimir

//        $this->_setEmpresa(); //die;

        $filePath = $this->_getDirNfe().'/'.$this->empresa['pasta_ambiente'].'/entradas/'.$this->chave.'-nfe.xml';

//        $handle       = fopen($filePath, 'r');
//        $fileContents = fread($handle, filesize($filePath));
//        $fileContents = Formata::excluirAcentos($fileContents);

        header('Content-type: "text/xml"; charset="utf8"');
        header('Content-Disposition: attachment;filename='.$this->chave .'.xml');
        header('Content-Length: '.filesize($filePath));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        ob_clean();
        flush();
        readfile($filePath);
        exit;

    }


    /**
     * Retorna um especifico xml de carta de correção
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function getXmlCce()
    {

        $this->chave = $this->searchFields['chave'];
        $this->seqEvento = $this->searchFields['seq'];

//        $this->_setEmpresa(); //die;

        $arqXmlCce = $this->_getXmlCce();

        if(!isset($this->searchFields['tipo_retorno']) || $this->searchFields['tipo_retorno'] == 1) {

            header('Content-type: "text/xml"; charset="utf8"');
            header('Content-Disposition: attachment;filename='.$this->chave .'.xml');
            header('Content-Length: '.filesize($arqXmlCce));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            ob_clean();
            flush();
            readfile($arqXmlCce);
            exit;

        } else {

            $xml = file_get_contents($arqXmlCce);
            $retorno['xml'] = $xml;
            return $this->provide($retorno);
        }
    }

    /**
     * Envia o xml e a danfe por email para o consumidor
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function enviarEmailNFe()
    {

        $arrParam = json_decode($this->request->getRawBody(), true);
        $this->salvarLog('****NFe 4.0**** array enviar email consumidor:'.print_r($arrParam,true));

        if(!isset($arrParam['chave'])) {
            return $this->provide(Formulario::getDefaultMessages(ERRO,SELECIONAR,'Informe a Chave do documento a ser enviado por Email'));
        }

        $this->chave     = $arrParam['chave'];
        $this->seqEvento = (isset($arrParam['seq'])) ? $arrParam['seq'] : 1;
        $arrParam['situacao'] = (isset($arrParam['situacao'])) ? $arrParam['situacao'] : 'autorizado';

        $retorno = Formulario::getDefaultMessages(SUCESSO,SELECIONAR,'Email enviado com Sucesso!');

        switch($arrParam['situacao']) {

            case 'cancelado':

                $id = $this->_gerarDanfeEvento('cancelado');
                $pdfFile = $this->_getDirNfe().'/danfe/'.$id.'.pdf';
                $this->daevento->printDocument($pdfFile, 'F');
                chmod($pdfFile, 0777);

                $xmlFile = $this->_getXmlCancelado();

                break;

            default:

                $pdfFile = $this->_getDirNfe().'/danfe/'.$this->chave.'.pdf';
                if(!file_exists($pdfFile)) {

                    $pdf = $this->_gerarDanfe();
                    file_put_contents($pdfFile,$pdf);
                    chmod($pdfFile, 0777);
                }

                $xmlFile = $this->_getXmlAprovado();
                $xmlCCe = $this->_getXmlCce();
                if(file_exists($xmlCCe)) {
                    $xmlFile = $xmlCCe;

                    $id = $this->_gerarDanfeEvento('cce');
                    $pdfFile = $this->_getDirNfe().'/danfe/'.$id.'-cce.pdf';
                    $this->daevento->printDocument($pdfFile, 'F');
                    chmod($pdfFile, 0777);
                }

                break;
        }

        $this->salvarLog('****NFe 4.0**** definiu XML para envio :'.$xmlFile);
        $this->salvarLog('****NFe 4.0**** definiu PDF para envio :'.$pdfFile);

        $config = new \stdClass();
//        $config->host      = 'smtp.test.com.br';
//        $config->user      = 'usuario@test.com.br';
//        $config->password  = 'senha';
//        $config->secure    = 'tls';
//        $config->port      = 587;
//        $config->from      = 'usuario@test.com.br';
//        $config->fantasy   = 'Test Ltda';
//        $config->replyTo   = 'vendas@test.com.br';
//        $config->replyName = 'Vendas';

        $config->host      = 'email-smtp.us-west-2.amazonaws.com';
        $config->user      = 'AKIAJPGQKO223KJU72ZQ';
        $config->password  = 'Ak5Mecu5xaPzEBWLV1pRA8eKB54Or7Tno1yeXCxZdQ/E';
        $config->secure    = 'tls';
        $config->port      = 587;
        $config->from      = $this->empresa['emailEmitente'];
        $config->replyTo   = $this->empresa['emailEmitente'];
        $config->fantasy   = $this->empresa['nomeFantasiaEmitente'];
        $config->replyName = $this->empresa['nomeFantasiaEmitente'];

        if(file_exists($xmlFile)) {

            try {
                //a configuração é uma stdClass com os campos acima indicados
                //esse parametro é OBRIGATÓRIO
                $mail = new Mail($config);

                //use isso para inserir seu próprio template HTML com os campos corretos
                //para serem substituidos em execução com os dados dos xml
                $htmlTemplate = '';
                $mail->loadTemplate($htmlTemplate);
                //aqui são passados os documentos, tanto pode ser um path como o conteudo
                //desses documentos
                $xml = $xmlFile;
                $pdf = (file_exists($pdfFile)) ? $pdfFile : '';//não é obrigatório passar o PDF, tendo em vista que é uma BOBAGEM
                $mail->loadDocuments($xml, $pdf);

                //se não for passado esse array serão enviados apenas os emails
                //que estão contidos no XML, isto se existirem

                $addresses = [];

                if(isset($arrParam['emails'])) {

                    $arrEmails = explode(',',$arrParam['emails']);

                    foreach ($arrEmails as $email) {
                        $addresses[] = $email;
                    }
                }

                $this->salvarLog('****NFe 4.0**** vai enviar os emails para :'.print_r($addresses,true));

                //envia emails, se false apenas para os endereçospassados
                //se true para todos os endereços contidos no XML e mais os indicados adicionais
                $mail->send($addresses, !empty($addresses));

            } catch (\InvalidArgumentException $e) {
                $retorno = Formulario::getDefaultMessages(ERRO,SELECIONAR,"Falha: " . $e->getMessage());
            } catch (\RuntimeException $e) {
                $retorno = Formulario::getDefaultMessages(ERRO,SELECIONAR,"Falha: " . $e->getMessage());
            } catch (\Exception $e) {
                $retorno = Formulario::getDefaultMessages(ERRO,SELECIONAR,"Falha: " . $e->getMessage());
            }
        } else {

            $retorno = Formulario::getDefaultMessages(ERRO,SELECIONAR,"Falha: O Arquivo passado não foi localizado!");
            $this->salvarLog('****NFe 4.0**** definiu PDF para envio :'.$pdfFile);
        }

        return $this->provide($retorno);
    }

    /**
     *
     */
    protected function _validarEntradasIniciais()
    {
        $msg = false;

        $arqCertificado = $this->_getDirNfe()."/certs/{$this->empresa['emp_nfe_cert_arq']}";

        if((empty($this->empresa['emp_nfe_cert_arq']) OR !file_exists($arqCertificado))) {
            $msg .= 'O arquivo do certificado digital ainda n&atilde;o foi configurado <br>';
        }

        if(empty(trim($this->empresa['CNPJEmitente']))) {
            $msg .= 'CNPJ da empresa emitente deve ser preenchido <br>';
        }

        if(empty(trim($this->empresa['nomeFantasiaEmitente']))) {
            $msg .= 'Nome fantasia da empresa emitente deve ser preenchido <br>';
        }

        if(empty(trim($this->empresa['nroEmitente']))) {
            $msg .= 'Nro do endereço da empresa emitente deve ser preenchido <br>';
        }
        if(empty(trim($this->empresa['xLgrEmitente']))) {
            $msg .= 'Endere&ccedil;o da empresa emitente deve ser preenchido <br>';
        }
        if(empty(trim($this->empresa['CEPEmitente']))) {
            $msg .= 'CEP da empresa emitente deve ser preenchido <br>';
        }

        if((!$this->notaExterior) AND (empty($this->arrEnviar['cpfDest']) AND empty($this->arrEnviar['cnpjDest']))) {
            $msg .= 'CPF ou CNPJ do Cliente deve ser preenchido <br>';

        }

        if(empty($this->arrEnviar['xLgrDest'])) {
            $msg .= 'Endere&ccedil;o do Cliente deve ser preenchido <br>';
        }

        if(!$this->notaExterior AND empty($this->arrEnviar['cMunDest'])) {
            $msg .= 'Cidade do Cliente deve ser preenchido <br>';
        }

        if(!$this->notaExterior AND empty($this->arrEnviar['xBairroDest'])) {
            $msg .= 'Bairro do Cliente deve ser preenchido <br>';
        }

        if((!$this->notaExterior) AND (!empty($this->arrEnviar['CEPDest']) AND strlen($this->arrEnviar['CEPDest']) != 8)) {
            $msg .= 'CEP do Cliente no formato incorreto <br>';

        }

        if($msg) {

            $this->codSefaz    = NFE_ERRO;
            $this->strMotivo   = $msg;
            $this->codSituacao = NFE_ALERTA;
            $this->msg         = $msg;
            $this->error       = true;
        }

        return $msg;
    }

    /**
     * Consulta um determinado recibo
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    function consultarDFe()
    {

        $arrParam = json_decode($this->request->getRawBody(), true);

        $this->salvarLog('****NFe 4.0**** inicio metodo vai consultar a nfe:'.print_r($arrParam,true));

        $dataAtual = date('Y-m-d H:i:s');
        $ultNSU = 0;

        $this->_checkDirExists($this->_getDirNfe() . "/extras/");

        # Verifica se existe log de consulta, caso exista, verifica se o intervalo da consulta está sendo respeitado:
        $fileUltimoDfe = $this->_getDirNfe() . "/extras/ult_dfe.json";

        if(file_exists($fileUltimoDfe)) {

            $jsonFileContent = file_get_contents($fileUltimoDfe);

            $arrDados = json_decode($jsonFileContent, true);

            $this->salvarLog('****NFe 4.0**** existe arquivo de log da ultima consulta:'.print_r($arrDados,true));

            $dataUltDfe = $arrDados['dataUltDFe'];
            $ultNSU     = $arrDados['ultNSU'];

            $diffDate = Formata::retornarDiferencaIntervaloHoras($dataUltDfe,$dataAtual);

            if($diffDate < 3) {
                return $this->provide(Formulario::getDefaultMessages(ERRO,SELECIONAR,'Prazo da ultima consulta ainda não excedeu as 3 horas recomendadas, tente mais tarde!'));
            }
        }

        try {
            # Consulta os dados do DFe:
            $retorno = $this->_consultarDFe($ultNSU);
        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        # Remove o arquivo da ultima consulta para que seja gerado um atualizado:
        if(file_exists($fileUltimoDfe)) {
            unlink($fileUltimoDfe);
        }

        $this->salvarLog('****NFe 4.0**** terminou processamento DFe:'.print_r($retorno,true));

        if(isset($retorno['ultNSU'])) {
            $fileContent['ultNSU'] = $retorno['ultNSU'];
        }

        $fileContent['dataUltDFe'] = date('Y-m-d H:i:s');

        # Grava a data atual no arquivo de log para que ser utilizado na próxima consulta:
        file_put_contents($fileUltimoDfe,json_encode($fileContent));
        chmod($fileUltimoDfe,0777);

        $this->salvarLog('****NFe 4.0**** Gera um arquivo de log com os dados atualizados :'.print_r($fileContent,true));

        return $this->provide(array_merge(Formulario::getDefaultMessages(SUCESSO,SELECIONAR,'Rotina DFe executada!'), $retorno));
    }


    protected function _consultarDFe($ultNSU)
    {

        try {

            $nfe = new Tools($this->configuraNFe(), $this->_getCertificado());
            $nfe->model('55');
            # Passa o ambiente para produção, ATENÇÃO COM ISSO!!!
            $nfe->setEnvironment(1);

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        $this->salvarLog('****NFe 4.0**** entra no método:'.__METHOD__);

        $dirDfe = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/dfe/";
        $this->_checkDirExists($dirDfe);

        $dirProtocolados = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/dfe/protocolados/";
        $this->_checkDirExists($dirProtocolados);

        //este numero deverá vir do banco de dados nas proximas buscas para reduzir
        //a quantidade de documentos, e para não baixar várias vezes as mesmas coisas.
        $maxNSU = $ultNSU;
        $loopLimit = 50;
        $iCount = 0;

        $retorno['arrResumo'] = [];

        //executa a busca de DFe em loop
        while ($ultNSU <= $maxNSU) {
            $iCount++;
            if ($iCount >= $loopLimit) {
                break;
            }

            try {
                $resp = $nfe->sefazDistDFe($ultNSU);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }

            //extrair e salvar os retornos
            $dom = new \DOMDocument();
            $dom->loadXML($resp);
            $node   = $dom->getElementsByTagName('retDistDFeInt')->item(0);
            $ultNSU = $node->getElementsByTagName('ultNSU')->item(0)->nodeValue;
            $maxNSU = $node->getElementsByTagName('maxNSU')->item(0)->nodeValue;
            $lote   = $node->getElementsByTagName('loteDistDFeInt')->item(0);

            if (empty($lote)) {
                //lote vazio
                continue;
            }

            //essas tags irão conter os documentos zipados
            $docs = $lote->getElementsByTagName('docZip');
            foreach ($docs as $doc) {

                $numnsu = $doc->getAttribute('NSU');
                $schema = $doc->getAttribute('schema');
                //descompacta o documento e recupera o XML original
                $content = gzdecode(base64_decode($doc->nodeValue));
                //identifica o tipo de documento
                $tipo = substr($schema, 0, 6);

                $this->salvarLog('****NFe 4.0**** Conteúdo do arquivo!! :'.print_r($content, true));
                $this->salvarLog('****NFe 4.0**** Tipo do arquivo!! :'.print_r($tipo, true));

                # Se for do tipo Resumo NFe, grava o resumo para consulta e armazena no array de retorno.
                # Se for documento processado, salva para ser utilizado:
                if($tipo == 'resNFe') {

                    $dom = new \DOMDocument();
                    $dom->loadXML($content);
                    $node   = $dom->getElementsByTagName('resNFe')->item(0);
                    $chave = $node->getElementsByTagName('chNFe')->item(0)->nodeValue;

                    $retorno['arrResumo'][] = [
                        'chaveNFe'     => $chave,
                        'CNPJEmitente' => $node->getElementsByTagName('CNPJ')->item(0)->nodeValue,
                        'nomeEmitente' => $node->getElementsByTagName('xNome')->item(0)->nodeValue,
                        'valorNFe'     => $node->getElementsByTagName('vNF')->item(0)->nodeValue,
                        'protocolo'    => $node->getElementsByTagName('nProt')->item(0)->nodeValue,
                        'cSitNFe'      => $node->getElementsByTagName('cSitNFe')->item(0)->nodeValue
                    ];

                    file_put_contents($dirDfe . $chave . '.xml', $content);
                    chmod($dirDfe . $chave . '.xml', 0777);

                } else if($tipo == 'procNF') {

                    $dom = new \DOMDocument();
                    $dom->loadXML($content);
                    $node  = $dom->getElementsByTagName('infNFe')->item(0);
                    $chave = substr($node->getAttribute('Id'),3,44);

                    file_put_contents($dirProtocolados . $chave .'.xml', $content);
                    chmod($dirProtocolados . $chave .'.xml', 0777);
                }
            }

            sleep(3);
        }

        # Retorna o ultim NSU para ser utilizado no próximo processamento:
        $retorno['ultNSU'] = $ultNSU;

        # Armazena o resumo em log
        if(count($retorno['arrResumo']) > 0) {

            $dirLogDFe = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/dfe/log/";
            $this->_checkDirExists($dirLogDFe);

            $logName = $dirLogDFe.'log.json';

            if(file_exists($logName)) {
                unlink($logName);
            }

            file_put_contents($logName, json_encode($retorno['arrResumo']));
            chmod($logName, 0777);
        }

        return $retorno;
    }

    /**
     * retorna a data de validade do certificado
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function getDataValidadeCertificado()
    {
//        $this->_setEmpresa();

        try {
            $cert = $this->_getCertificado();
        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }

        $retorno = Formulario::getDefaultMessages(SUCESSO, SELECIONAR);
        $retorno['validade'] = $cert->getValidTo()->format('d/m/Y H:i:s');
        $retorno['expirado'] = $cert->isExpired();

        return $this->provide($retorno);
    }

    /**
     * @param $dir
     */
    protected function _checkDirExists($dir)
    {

        $this->salvarLog(" Checa se o diretório existe :". $dir);

        if(!file_exists($dir)) {
            mkdir($dir, 0777, true);
            chmod($dir, 0777);
        }
    }

    /**
     * Valida se os dados principais da nota foram passados
     * @param $arrParams
     * @throws \Exception
     */
    protected function _validaParametrosEntrada($arrParams)
    {

        if(!isset($arrParams['destinatario'])) {
            throw new \Exception('Faltou informar os dados do Destinatario');
        }

        if(!isset($arrParams['nota'])) {
            throw new \Exception('Faltou informar os dados da Nota a ser emitida');
        }

        if(!isset($arrParams['nota']['itens'])) {
            throw new \Exception('Faltou informar os Itens da Nota a ser emitida');
        }
    }

    /**
     * Popula as variáveis de retorno em caso de excessão
     * @param \Exception $e
     */
    protected function _handleException(\Exception $e)
    {

        $this->salvarLog('****NFe 4.0**** EXCESSÃO OCORRIDA NO MÉTODO: '. $e->getMessage());

        $this->codSefaz    = NFE_ERRO;
        $this->strMotivo   = $e->getMessage();
        $this->codSituacao = NFE_ALERTA;
        $this->msg         = $e->getMessage();
        $this->error       = true;

        $this->_setRetornoCliente();
    }

    /**
     * Baixa um xml de uma nota
     * @param string $folder
     */
    public function downloadXml($ev = 'autorizado')
    {

        $arrParam = $this->searchFields;

        $this->chave = $arrParam['chave'];

        switch ($ev)
        {

            case 'entrada':
                $apend = '-nfe';
                $arqXml = $this->_getXmlEntrada();
                break;

            case 'cancelado':
                $apend = '-CancNFe-procEvento';
                $arqXml = $this->_getXmlCancelado();
                break;

            case 'cce':
                if(!isset($arrParam['seq'])) {
                    die('informe a sequencia da carta de correção que deseja baixar!');
                }

                $this->seqEvento = $arrParam['seq'];
                $apend = '-CCe-procEvento';
                $arqXml = $this->_getXmlCce();
                break;

            default:
                $apend = '-nfe';
                $arqXml = $this->_getXmlAprovado();
                break;
        }

        if(!file_exists($arqXml)) {
            die('O Arquivo solicitado não existe!');
        }

        $this->salvarLog("Vai baixar o XML: >> $arqXml");

        $fileName = $this->chave . $apend;

        header('Content-type: "text/xml"; charset="utf8"');
        header("Content-Disposition: attachment;filename=$fileName.xml");
        header('Content-Length: '.filesize($arqXml));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        ob_clean();
        flush();
        readfile($arqXml);
        exit;
    }

    /**
     * Manifesta um lote de notas fiscais passadas como parametro
     * 210210 Evento ciencia da operação;
     * 210240 Evento não realizada; - NESSE EVENTO, A JUSTIFICATIVA É OBRIGATÓRIA
     * 210200 Evento confirmação;
     * 210220 Evento de Operação não Realizada
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function manifestaLote()
    {

        $arrParam = json_decode($this->request->getRawBody(), true);

        if(empty($arrParam) OR !is_array($arrParam)) {
            return $this->provide(Formulario::getDefaultMessages(ERRO, ATUALIZAR, ['Informe os dados a serem processados!']));
        }

        try {
            $tools = new Tools($this->configuraNFe(), $this->_getCertificado());
            $tools->model('55');

            $std = new \stdClass();

            foreach ($arrParam as $k => $param)
            {

                $std->evento[$k] =  new \stdClass();
                $std->evento[$k]->chNFe      = $param['chave'];
                $std->evento[$k]->tpEvento   = $param['cod_evento'];
                $std->evento[$k]->xJust      = (!empty($param['justificativa'])) ? $param['justificativa'] : null;
                $std->evento[$k]->nSeqEvento = $param['seq'];
            }

            $response = $tools->sefazManifestaLote($std);

            $std = new Standardize($response);

            $stdRes  = $std->toStd();
            $retorno = [];

            if(isset($stdRes->retEvento) AND count($stdRes->retEvento) > 0) {

                foreach ($stdRes->retEvento as $retEvento)
                {

                    $retorno[] = [
                        'codSefaz'  => $retEvento->infEvento->cStat,
                        'strMotivo' => $retEvento->infEvento->xMotivo,
                        'msg'       => $retEvento->infEvento->xMotivo,
                        'error'     => preg_match('/Rejeicao/',$retEvento->infEvento->xMotivo),
                        'chave'       => $retEvento->infEvento->chNFe,
                    ];
                }
            }

            return $this->provide($retorno);

        } catch (\Exception $e) {

            $this->_handleException($e);
            return $this->provide($this->retornoCliente);
        }
    }

    /**
     * Baixa o XML Manifestado junto a receita
     * @return \OrganicRest\Responses\Schemas\Rest
     */
    public function downloadXmlTerceiro($chave)
    {

        if(strlen($chave) < 44) {
            echo "Informe a Chave da Nota que deseja baixar o XML!";
            die;
            /**
                return $this->provide(Formulario::getDefaultMessages(ERRO, ATUALIZAR, ['Informe a Chave da Nota que deseja baixar o XML!']));
             */
        }

        $this->salvarLog('****NFe 4.0**** Entra para baixar o xml da chave: '. $chave);

        try {
            $tools = new Tools($this->configuraNFe(), $this->_getCertificado());
            $tools->model('55');
            $tools->setEnvironment(1);

            $response = $tools->sefazDownload($chave);

            $stz = new Standardize($response);
            $std = $stz->toStd();

            if ($std->cStat != 138) {

                /**
                    $retorno = [
                    'codSefaz'  => $std->cStat,
                    'strMotivo' => $std->xMotivo,
                    'msg'       => $std->xMotivo,
                    'error'     => true,
                    'chave'     => $chave
                    ];

                    return $this->provide($retorno);
                 */

                echo "Documento não retornado. [$std->cStat] $std->xMotivo";
                die;
            }

            $this->salvarLog("****NFe 4.0**** Retornou do sefazDownload: $std->cStat - $std->xMotivo");

            $zip = $std->loteDistDFeInt->docZip;
            $xml = gzdecode(base64_decode($zip));

            $dirProtocolados = $this->_getDirNfe()."/{$this->empresa['pasta_ambiente']}/dfe/protocolados/";
            $this->_checkDirExists($dirProtocolados);

            $filePath = $dirProtocolados . $chave .'.xml';

            file_put_contents($filePath, $xml);
            chmod($filePath, 0777);

            header('Content-type: "text/xml"; charset="utf8"');
            header('Content-Disposition: attachment;filename='.$chave .'.xml');
            header('Content-Length: '.filesize($filePath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            ob_clean();
            flush();
            readfile($filePath);
            exit;

        } catch (\Exception $e) {

            echo str_replace("\n", "<br/>", $e->getMessage());
            /**
                $this->_handleException($e);
                return $this->provide($this->retornoCliente);
             */
        }
    }
}
